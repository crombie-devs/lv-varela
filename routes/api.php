<?php

use Illuminate\Http\Request;

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\ContactoController;
use App\Http\Controllers\Api\UsuarioMobileController;
use App\Http\Controllers\Api\BeneficioController;
use App\Http\Controllers\Api\OfertaController;
use App\Http\Controllers\Api\CuponController;

use App\Http\Resources\BannerResource;
use App\Http\Resources\BeneficioResource;
use App\Http\Resources\BeneficioUsuarioResource;
use App\Http\Resources\OfertaResource;
use App\Http\Resources\OfertaUsuarioResource;
use App\Http\Resources\SucursalResource;
use App\Http\Resources\ComercioResource;
use App\Http\Resources\CategoriaResource;
use App\Http\Resources\UsuarioMobileResource;
use App\Http\Resources\CuponResource;
use App\Http\Resources\CanjeableResource;

use App\Models\Banner;
use App\Models\Beneficio;
use App\Models\BeneficioUsuario;
use App\Models\Oferta;
use App\Models\OfertaUsuario;
use App\Models\Sucursal;
use App\Models\Comercio;
use App\Models\Categoria;
use App\Models\UsuarioMobile;
use App\Models\Cupon;
use App\Models\Canjeable;

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

Route::apiResource('/contactos', ContactoController::class)->middleware('auth:api');
Route::apiResource('/usuarios-app', UsuarioMobileController::class)->middleware('auth:api');

Route::apiResource('/beneficios-qr/canjear', BeneficioController::class)->middleware('auth:api');
Route::get('/beneficios-qr/halcoins/{beneficio_id}/{usuario_id}', [BeneficioController::class, 'halcoinsAcum'])->name('halcoins.halcoinsAcum');

Route::apiResource('/ofertas-qr/canjear', OfertaController::class)->middleware('auth:api');
Route::get('/ofertas-qr/halcoins/{beneficio_id}/{usuario_id}', [OfertaController::class, 'halcoinsAcum'])->name('halcoins.halcoinsAcumOferta');

Route::apiResource('/cupones', CuponController::class)->middleware('auth:api');
// Route::get('/cupones/deleted/{id}', [CuponController::class, 'deleted'])->name('cupones.deleted');
// Route::get('/cupones/redeem/{id}', [CuponController::class, 'redeem'])->name('cupones.redeem');
Route::post('/cupones/canjear', [CuponController::class, 'canjear'])->name('cupones.canjear');

Route::get('banners', function () {
    return BannerResource::collection(Banner::all());
});

Route::get('banners/{id}', function ($id) {
    return new BannerResource(Banner::findOrFail($id));
});

Route::get('beneficios', function () {
    return BeneficioResource::collection(Beneficio::all());
});

Route::get('beneficios/{id}', function ($id) {
    return new BeneficioResource(Beneficio::findOrFail($id));
});

Route::get('ofertas', function () {
    return OfertaResource::collection(Oferta::all());
});

Route::get('ofertas/{id}', function ($id) {
    return new OfertaResource(Oferta::findOrFail($id));
});

Route::get('sucursales', function () {
    return SucursalResource::collection(Sucursal::all());
});

Route::get('sucursales/{id}', function ($id) {
    return new SucursalResource(Sucursal::findOrFail($id));
});

Route::get('comercios', function () {
    return ComercioResource::collection(Comercio::all());
});

Route::get('comercios/{id}', function ($id) {
    return new ComercioResource(Comercio::findOrFail($id));
});

Route::get('categorias', function () {
    return CategoriaResource::collection(Categoria::all());
});

Route::get('categorias/{id}', function ($id) {
    return new CategoriaResource(Categoria::findOrFail($id));
});

Route::get('usuarios/mobile', function () {
    return UsuarioMobileResource::collection(UsuarioMobile::all());
});

Route::get('usuarios/mobile/{id}', function ($id) {
    return new UsuarioMobileResource(UsuarioMobile::findOrFail($id));
});

Route::get('canjeables', function () {
    return CanjeableResource::collection(Canjeable::all());
});

Route::get('canjeables/{id}', function ($id) {
    return new CanjeableResource(Canjeable::findOrFail($id));
});

// Route::get('cupones', function () {
//     return CuponResource::collection(Cupon::all());
// });

// Route::get('cupones/{id}', function ($id) {
//     return new CuponResource(Cupon::findOrFail($id));
// });

Route::get('beneficios-usuarios', function () {
    return BeneficioUsuarioResource::collection(BeneficioUsuario::all());
});

Route::get('beneficios-usuarios/{id}', function ($id) {
    return new BeneficioUsuarioResource(BeneficioUsuario::findOrFail($id));
});

Route::get('ofertas-usuarios', function () {
    return OfertaUsuarioResource::collection(OfertaUsuario::all());
});

Route::get('ofertas-usuarios/{id}', function ($id) {
    return new OfertaUsuarioResource(OfertaUsuario::findOrFail($id));
});