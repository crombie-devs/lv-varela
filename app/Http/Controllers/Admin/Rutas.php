<?php

namespace App\Http\Controllers\Admin;

class Rutas {

    const CUPON_INDEX = 'admin/cupones';
    const COMERCIO_INDEX = 'admin/comercios';
    const OFERTA_INDEX = 'admin/ofertas';
    const BENEFICIO_INDEX = 'admin/beneficios';

}