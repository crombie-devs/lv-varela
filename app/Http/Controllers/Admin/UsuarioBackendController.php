<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Comercio;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UsuarioBackendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['entities'] = User::all();

        return view('admin.usuarios.backend.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = [
            'ROLE_ADMIN' => 'ROLE_ADMIN',
            'ROLE_COMERCIO' => 'ROLE_COMERCIO',
        ];
        $comercios = Comercio::pluck('nombre', 'id');

        return view('admin.usuarios.backend.create', [
            'roles' => $roles,
            'comercios' => $comercios
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entity = request()->except('_token');
        $entity['password'] = Hash::make($request->password);

        if ($entity['rol'] == "ROLE_ADMIN") {
            $user = new User($entity);
            $user->save();
            $user->assignRole('ROLE_ADMIN');
            $user->save();
            // User::findOrFail($user->id());
            // User::insert($entity)
        } else {
            $user = new User($entity);
            $user->save();
            $user->assignRole('ROLE_COMERCIO');
            $user->save();
        }

        return redirect('admin/usuarios/backend')->with("message", "Usuario agregado!.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entity = User::findOrFail($id);
        $roles = [
            'ROLE_ADMIN' => 'ROLE_ADMIN',
            'ROLE_COMERCIO' => 'ROLE_COMERCIO',
        ];
        $comercios = Comercio::pluck('nombre', 'id');
        // $comercios->prepend('Sin Comercio', null);

        return view('admin.usuarios.backend.edit', [
            'entity' => $entity,
            'roles' => $roles,
            'comercios' => $comercios
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $entity = request()->except(['_token', '_method']);

        User::where('id', '=', $id)->update($entity);
        $user = User::findOrFail($id);
        $user->roles()->detach();

        if ($entity['rol'] == "ROLE_ADMIN") {
            $user->assignRole('ROLE_ADMIN');
            $user->save();
        } else {
            $user->assignRole('ROLE_COMERCIO');
            $user->save();
        }

        return redirect('admin/usuarios/backend')->with("message", "Usuario actualizado!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function deleted($id)
    {
        $entity = User::findOrFail($id);
        if ($entity) {
            User::destroy($id);
        }

        return redirect('admin/usuarios/backend')->with("message", "Usuario borrado!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);

        return redirect('admin/usuarios/backend')->with("message", "Usuario borrado!.");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function changePassword($id) 
    {
        return view('admin.usuarios.backend.change-password', [
            'id' => $id,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request, $id)
    {
        $request->validate([
            'old_password' => 'required|min:6|max:100',
            'new_password' => 'required|min:6|max:100',
            'confirm_password' => 'required|same:new_password',
        ]);

        $current_user = User::findOrFail($id);

        if (Hash::check($request->old_password, $current_user->password)) {

            $current_user->update([
                'password' => bcrypt($request->new_password)
            ]);

            return redirect()->back()->with("message", "Contraseña actualizada.");

        } else {
            return redirect()->back()->with("message", "La contraseña anterior no coincide.");
        }
    }
}
