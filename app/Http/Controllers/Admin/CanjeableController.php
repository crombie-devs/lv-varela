<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Models\Canjeable;
use App\Models\Sucursal;
use App\Models\CanjeableSucursal;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CanjeableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entities = Canjeable::all();

        // si hay canjeables sin sucursales las desactivo.
        foreach ($entities as $canjeable) {
            if (count($canjeable->sucursales) == 0) {
                $ca = Canjeable::findOrFail($canjeable->id);
                Canjeable::destroy($ca->id);
                return redirect('admin/canjeables');
            }
        }

        return view('admin.canjeable.index', [
            'entities' => $entities,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sucursales = Sucursal::pluck('nombre', 'id');
        
        return view('admin.canjeable.create', [
            'sucursales' => $sucursales
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entity = Canjeable::create($request->all());

        $id = DB::getPdo()->lastInsertId();
        foreach ($request->sucursales as $sucursal) {
            $canjeableSucursal = new CanjeableSucursal;
            $canjeableSucursal->canjeable_id = $id;
            $canjeableSucursal->sucursal_id = $sucursal;
            $canjeableSucursal->timestamps = false;
            $canjeableSucursal->save();
        }

        return redirect('admin/canjeables')->with("message", "Canjeable agregado!.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Canjeable  $canjeable
     * @return \Illuminate\Http\Response
     */
    public function show(Canjeable $canjeable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Canjeable  $canjeable
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entity = Canjeable::findOrFail($id);
        $sucursales = Sucursal::pluck('nombre', 'id');

        return view('admin.canjeable.edit', [
            'entity' => $entity,
            'sucursales' => $sucursales
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Canjeable  $canjeable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $entity = request()->except(['_token', '_method']);
        unset($entity['sucursales']);

        CanjeableSucursal::where('canjeable_id', $id)->delete();

        foreach ($request->sucursales as $sucursal) {
            $canjeableSucursal = new CanjeableSucursal;
            $canjeableSucursal->canjeable_id = $id;
            $canjeableSucursal->sucursal_id = $sucursal;
            $canjeableSucursal->timestamps = false;
            $canjeableSucursal->save();
        }

        Canjeable::where('id', '=', $id)->update($entity);

        return redirect('admin/canjeables')->with("message", "Canjeable actualizado!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Canjeable $canjeable
     * @return \Illuminate\Http\Response
     */
    public function deleted($id)
    {
        $entity = Canjeable::findOrFail($id);
        if ($entity) {
            Canjeable::destroy($id);
        }

        return redirect('admin/canjeables')->with("message", "Canjeable borrado!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Canjeable  $canjeable
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Canjeable::destroy($id);

        return redirect('admin/canjeables')->with("message", "Canjeable borrado!.");
    }

    /**
     * Get QR Code View.
     *
     * @param  \App\Canjeable  $canjeable
     * @return \Illuminate\Http\Response
     */
    public function showQR($id)
    {
        return view('admin.canjeable.qr', [
            'id' => $id
        ]);
    }

    /**
     * print PDF View.
     *
     * @param  \App\Canjeable  $canjeable
     * @return \Illuminate\Http\Response
     */
    public function printPDF($id)
    {
        $pdf = app('dompdf.wrapper');
        $pdf->loadHTML('<h1>Varela + Cerca</h1>');

        return $pdf->download('mi-archivo.pdf');
    }
}
