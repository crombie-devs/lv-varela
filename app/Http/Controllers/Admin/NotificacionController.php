<?php

namespace App\Http\Controllers\Admin;

use App\Models\Notificacion;
use App\Models\UsuarioMobile;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

// use App\Traits\NotificationsFirebase;
// use App\Entities\Benefit;
// use App\Entities\CityHall;
// use App\Entities\Recess;
// use App\Entities\RecessCategory;
// use App\Entities\User;
// use App\Http\Requests\Backend\NotificationsSendRequest;
// use App\Repositories\RecessSimpleRepository;
// use Carbon\Carbon;
// use Illuminate\Support\Facades\Gate;
// use Illuminate\Support\Facades\Redirect;
// use Illuminate\Support\Facades\Storage;
// use Illuminate\Support\Collection;
// use GuzzleHttp\Client;

class NotificacionController extends Controller
{

    // use NotificationsFirebase;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['entities'] = Notificacion::all();

        return view('admin.notificacion.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $usuariosMobile = UsuarioMobile::all();

        return view('admin.notificacion.create', [
            'usuariosMobile' => $usuariosMobile
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entity = request()->except('_token');
        $entity['fecha'] = new \DateTime();

        if ($entity['usuario_id'] == 'todos') {

            $usuarios = UsuarioMobile::all();
            
            foreach ($usuarios as $key => $usuario) {

                if (!$usuario->device_key) {
                    return redirect('admin/notificaciones/create')->with("messageError", "Se debe guardar primero el ID Dispositivo de todos los Usuarios antes de poder mandar un mensaje masivo!.");
                }

                $url = 'https://fcm.googleapis.com/fcm/send';
                $FcmToken = $usuario->device_key;
                
                $serverKey = 'AAAAmGczkTw:APA91bGMrhMx7K_iyUpsOJKukcI5EltcOjSjVNgL2gqZ0Gr5Mxq3_YKOip2Y594AbSC4W5ojb2g38EC-Mmvs8bQsP_v7G-eeOX8W62QA96SfTPsFk6QqvDWQDhgOc1xIOw1vOZV9mNHV';
                
                $data = [
                    "registration_ids" => array($FcmToken),
                    "notification" => [
                        "title" => 'Nueva notificación',
                        "body" => $entity['mensaje'],
                    ]
                ];

                $encodedData = json_encode($data);
                
                $headers = [
                    'Authorization:key=' . $serverKey,
                    'Content-Type: application/json',
                ];
            
                $ch = curl_init();
            
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                // Disabling SSL Certificate support temporarly
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);

                // Execute post
                $result = curl_exec($ch);

                if ($result === FALSE) {
                    die('Curl failed: ' . curl_error($ch));
                }        

                // Close connection
                curl_close($ch);

                Notificacion::insert($entity);
                
            }

            return redirect('admin/notificaciones/create')->with("message", "Notificaciones enviadas!.");
        } else {

            $usuario = UsuarioMobile::findOrFail($entity['usuario_id']);

            if (!$usuario->device_key) {
                return redirect('admin/notificaciones/create')->with("messageError", "Se debe guardar primero el ID Dispositivo del Usuario!.");
            }
            

            $url = 'https://fcm.googleapis.com/fcm/send';
            $FcmToken = $usuario->device_key;
            
            // $serverKey = 'AAAA-OGc1YA:APA91bEBr3_hgTc9n5trbHpEkoDWYWgru0IEsDK1yIlg6U9KJYr35eSoaK2VzzGmihAZFsD-GH3NQT9jKcpclcKVcjxFQJjYmgvxLx97m3mVFXcND2FZvPL_11Xhfpnr6ZCqIZTdpaUV';
            $serverKey = 'AAAAmGczkTw:APA91bGMrhMx7K_iyUpsOJKukcI5EltcOjSjVNgL2gqZ0Gr5Mxq3_YKOip2Y594AbSC4W5ojb2g38EC-Mmvs8bQsP_v7G-eeOX8W62QA96SfTPsFk6QqvDWQDhgOc1xIOw1vOZV9mNHV';
            
            $data = [
                "registration_ids" => array($FcmToken),
                "notification" => [
                    "title" => 'Nueva notificación',
                    "body" => $entity['mensaje'],
                ]
            ];

            $encodedData = json_encode($data);
            
            $headers = [
                'Authorization:key=' . $serverKey,
                'Content-Type: application/json',
            ];
        
            $ch = curl_init();
        
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);

            // Execute post
            $result = curl_exec($ch);

            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }        

            // Close connection
            curl_close($ch);

            Notificacion::insert($entity);

            return redirect('admin/notificaciones/create')->with("message", "Notificación enviada!.");

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Notificacion  $notificacion
     * @return \Illuminate\Http\Response
     */
    public function show(Notificacion $notificacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Notificacion  $notificacion
     * @return \Illuminate\Http\Response
     */
    public function edit(Notificacion $notificacion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Notificacion  $notificacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notificacion $notificacion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Notificacion  $notificacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notificacion $notificacion)
    {
        //
    }

    public function storeTokenBrowser(Request $request)
    {
        $usuario = UsuarioMobile::findOrFail("mY4fghSJOyVacasdP87a2QLr32DPq");
        $usuario->device_key = $request->token;
        $usuario->save();

        return response()->json(['Token successfully stored.']);
    }

    // public function show()
    // {
    //     $this->authorize('show_notifications');

    //     $cityHalls_ = CityHall::orderBy('name');
    //     $cityHalls = $cityHalls_->pluck('name', 'id')->toArray();
    //     $recces = self::recessesSelect(request()->user());
    //     $recces['Sin Asignar Recreo'] = array('group'=>'Sin Asignar Recreo','items'=>[0=>'Sin Asignar Recreo']) ;
    //     $cityHalls[0] = 'Sin Asignar Municipio';
    //     $reccesCategories = RecessCategory::orderBy('name')->pluck('name', 'id')->toArray();

    //     return response(
    //         view('backend.notifications.show', [
    //             'recesses' => $recces,
    //             'cityHalls' => $cityHalls,
    //             'recessesCategories' => $reccesCategories,
    //         ])
    //     );
    // }

    // public function send(NotificationsSendRequest $request)
    // {
    //     $this->authorize('send_notifications');
    //     $data = [];
    //     $tokens = [];
    //     $users = new Collection();

    //     $path = null;
    //     if($request->hasFile('image')){
    //         $filename = time().'.'.$request->file('image')->getClientOriginalExtension();
    //         $request->file('image')->storeAs('notifications_send',$filename,['disk'=>'public']);
    //         $path = asset(sprintf('storage/%s/%s', 'notifications_send', $filename));
    //     }

    //     if($request->input('link')){
    //         $data['link'] = $request->input('link');
    //     }else{
    //         $data['link'] = "sin_link";
    //     }

    //     if($request->has('more_40')){
    //         $users = $this->filterUserByAge('more_40');
    //     }

    //     if($request->has('range_30_40')){
    //         $users = $users->merge($this->filterUserByAge('range_30_40'));
    //     }

    //     if($request->has('range_20_30')){
    //         $users = $users->merge($this->filterUserByAge('range_20_30'));
    //     }

    //     if($request->has('until_20')){
    //         $users = $users->merge($this->filterUserByAge('until_20'));
    //     }

    //     if($request->has('city_hall_id')){
    //        $users = $this->filterUserByCityHall($users, $request->get('city_hall_id'));
    //     }
         
    //     if($request->input('categories')){
    //         foreach ($users as $key=>$us){
    //             $a = json_decode($us->notifications->categories_recess);
    //             if($a){
    //                 $containsAllValues = !array_diff($request->input('categories'),$a);
    //                 if(!$containsAllValues)unset($users[$key]);
    //             }else{
    //               unset($users[$key]);
    //           }
    //         }
    //     }
        
    //     $users_andorid = $users->pluck('notifications.android_device_id')->toArray();
    //     $users_ios = $users->pluck('notifications.ios_device_id')->toArray();
    //     $tokens = array_merge($users_andorid,$users_ios);
    //     $tokens = array_filter($tokens, function($v) { return !is_null($v); });
        
    //     if($request->has('city_hall_id')){
    //         $data['city_hall_ids']= json_encode($request->input('city_hall_id'));
    //     }
    //     if($request->has('categories')){
    //         $data['categories']= json_encode($request->input('categories'));
    //     }
    //     if(!empty($tokens)){
    //         $this->sendNotification($request->input('title'),$request->input('description_text'),$path,$data,$tokens,$users->pluck('id'));    
    //     }
    //     session()->flash('success', trans('messages.notifications.actions.updated_success'));
    //     return Redirect::to(route('notifications.show'));
    // }

    // public static function recessesSelect(User $user, Recess $recessSimple = null): array
    // {
    //     $recessSimplees = [];
    //     $recessSimpleesTmp = Recess::with(['cityHall' => function ($query) {
    //         $query->orderBy('name');
    //     }])->where('is_recess_simple', '=', true)->where('is_interest_point','=',0)->orderBy('title');

    //     if ($recessSimple) {
    //         $recessSimpleesTmp->where('id', '!=', $recessSimple->id);
    //     }

    //     if (!$user->isAdmin() && $user->isCityHall()) {
    //         $recessSimpleesTmp->whereHas('cityHall', function ($query) use ($user) {
    //             $query->whereHas('user', function ($query2) use ($user) {
    //                 $query2->where('id', '=', $user->id);
    //             });
    //         });
    //     }

    //     foreach ($recessSimpleesTmp->get() as $recessSimple) {
    //         if (!isset($recessSimplees[$recessSimple->cityHall->name])) {
    //             $recessSimplees[$recessSimple->cityHall->name] = [
    //                 'group' => $recessSimple->cityHall->name,
    //                 'items' => []
    //             ];
    //         }

    //         $recessSimplees[$recessSimple->cityHall->name]['items'][$recessSimple->id] = $recessSimple->title;
    //     }

    //     return $recessSimplees;
    // }

    // private function get_string_between($string, $start, $end){
    //     $string = ' ' . $string;
    //     $ini = strpos($string, $start);
    //     if ($ini == 0) return '';
    //     $ini += strlen($start);
    //     $len = strpos($string, $end, $ini) - $ini;
    //     return substr($string, $ini, $len);
    // }

    // private function filterUserByAge($age_range){
        
    //     $users = User::whereHas('notifications')
    //     ->whereHas('profileApp')
    //     ->with(['notifications','profileApp'])
    //     ->get();

    //     $filtered_users = [];

    //     switch ($age_range) {
    //         case 'more_40':
    //             $filtered_users = $users->filter(function($user){
    //                 return $user->profileApp->birth_day < Carbon::now()->subYears(40);
    //             });

    //             break;

    //         case 'range_30_40':
    //             $filtered_users = $users->filter(function($user){
    //                 return $user->profileApp->birth_day > Carbon::now()->subYears(41) && $user->profileApp->birth_day < Carbon::now()->subYears(31);
    //             });
                
    //             break;

    //         case 'range_20_30':
    //             $filtered_users = $users->filter(function($user){
    //                 return $user->profileApp->birth_day > Carbon::now()->subYears(31) && $user->profileApp->birth_day < Carbon::now()->subYears(21);
    //             });
                
    //             break;
            
    //         case 'until_20':
    //             $filtered_users = $users->filter(function($user){
    //                 return $user->profileApp->birth_day > Carbon::now()->subYears(20);
    //             });
                
    //             break;
            
    //         default:
    //             # code...
    //             break;
    //     }

    //     return $filtered_users;
    // }

    // private function filterUserByCityHall($users, $cityHalls){
    //     $filtered_users = $users->filter(function($user) use ($cityHalls){

    //         $user_city_halls = json_decode($user->notifications->city_hall);
            
    //         if(!empty($user_city_halls) && is_array($user_city_halls)){
    //             foreach ($user_city_halls as $cityHall) {
    //                 if(in_array($cityHall, $cityHalls)) return true;
    //             }
    //         }

    //         return false;
    //     });

    //     return $filtered_users;
    // }
}
