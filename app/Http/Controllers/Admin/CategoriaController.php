<?php

namespace App\Http\Controllers\Admin;

use App\Models\Categoria;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['entities'] = Categoria::all();

        return view('admin.categoria.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categoria.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entity = request()->except('_token');

        Categoria::insert($entity);

        return redirect('admin/categorias')->with("message", "Categoria agregada!.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function show(Categoria $categoria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entity = Categoria::findOrFail($id);

        return view('admin.categoria.edit', compact('entity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $entity = request()->except(['_token', '_method']);

        Categoria::where('id', '=', $id)->update($entity);

        return redirect('admin/categorias')->with("message", "Categoria actualizada!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Categoria $categoria
     * @return \Illuminate\Http\Response
     */
    public function deleted($id)
    {
        $entity = Categoria::findOrFail($id);
        if ($entity) {
            Categoria::destroy($id);
        }

        return redirect('admin/categorias')->with("message", "Categoria borrada!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Categoria::destroy($id);

        return redirect('admin/categorias')->with("message", "Categoria borrada!.");
    }
}
