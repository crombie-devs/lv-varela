<?php

namespace App\Http\Controllers\Admin;

use App\Models\UsuarioMobile;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

class UsuarioMobileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['entities'] = UsuarioMobile::all();

        return view('admin.usuarios.mobile.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = [
            'ROLE_APP' => 'ROLE_APP',
            'ROLE_COMERCIO' => 'ROLE_COMERCIO',
        ];

        return view('admin.usuarios.mobile.create', [
            'roles' => $roles,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entity = request()->except('_token');

        $active = ($entity['activo'] = 'on') ? 1 : 0;
        $entity['activo'] = $active;

        unset($entity['fecha_nac']);
        
        UsuarioMobile::insert($entity);

        return redirect('admin/usuarios/mobile')->with("message", "Usuario agregado!.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UsuarioMobile  $usuarioMobile
     * @return \Illuminate\Http\Response
     */
    public function show(UsuarioMobile $usuarioMobile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UsuarioMobile  $usuarioMobile
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entity = UsuarioMobile::findOrFail($id);
        $roles = [
            'ROLE_APP' => 'ROLE_APP',
            'ROLE_COMERCIO' => 'ROLE_COMERCIO',
        ];

        return view('admin.usuarios.mobile.edit', [
            'entity' => $entity,
            'roles' => $roles
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UsuarioMobile  $usuarioMobile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $entity = request()->except(['_token', '_method']);

        UsuarioMobile::where('id', '=', $id)->update($entity);

        return redirect('admin/usuarios/mobile')->with("message", "Usuario actualizado!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UsuarioMobile $usuarioMobile
     * @return \Illuminate\Http\Response
     */
    public function deleted($id)
    {
        $entity = UsuarioMobile::findOrFail($id);
        if ($entity) {
            UsuarioMobile::destroy($id);
        }

        return redirect('admin/usuarios/mobile')->with("message", "Usuario borrado!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UsuarioMobile  $usuarioMobile
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        UsuarioMobile::destroy($id);

        return redirect('admin/usuarios/mobile')->with("message", "Usuario borrado!.");
    }
}
