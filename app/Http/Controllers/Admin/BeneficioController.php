<?php

namespace App\Http\Controllers\Admin;

use App\Models\Beneficio;
use App\Models\BeneficioUsuario;
use App\Models\Sucursal;
use App\Models\Canjeable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class BeneficioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->rol == "ROLE_COMERCIO") {
            $beneficios = Beneficio::all();
            $entities = $beneficios;
            foreach ($beneficios as $key => $beneficio) {
                if (isset($beneficio->canjeable)) {
                    if (count($beneficio->canjeable->sucursales) > 0) {
                        $flag = false;
                        foreach ($beneficio->canjeable->sucursales as $sucursal) {
                            if ($sucursal->comercio_id == $user->comercio_id) {
                                $flag = true;
                            }
                        }
                        if ($flag = false) {
                            unset($entities[$key]);
                        }
                    }
                }
            }
        } else {    
            $entities = Beneficio::all();
        }

        // si hay beneficios sin canjeables las desactivo.
        foreach ($entities as $beneficio) {
            if (!$beneficio->canjeable) {
                $be = Beneficio::findOrFail($beneficio->id);
                $be->activa = 0;
                $be->save();
            }
        }

        return view('admin.beneficio.index', [
            'user' => $user,
            'entities' => $entities,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function canjeables()
    {
        $entities = BeneficioUsuario::all();
        
        return view('admin.beneficio.canjeables', [
            'entities' => $entities,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $canjeables = Canjeable::pluck('nombre', 'id');

        return view('admin.beneficio.create', [
            'user' => $user,
            'canjeables' => $canjeables
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entity = $request->except(['_token', '_method']);
        $activa = (isset($entity['activa']) && $entity['activa'] = 'on') ? 1 : 0;
        $entity['activa'] = $activa;
        $destacada = (isset($entity['destacada']) && $entity['destacada'] = 'on') ? 1 : 0;
        $entity['destacada'] = $destacada;
        unset($entity['imagen_cuadro']);
        unset($entity['imagen_cabecera']);

        if ($request->file('imagen_cuadro')) {
            $url = Storage::put('public/images/beneficios', $request->file('imagen_cuadro'));
            $entity['imagen_cuadro_url'] = $url;
            $entity['imagen_cuadro_src'] = str_replace('public', 'storage', $url);
        }

        if ($request->file('imagen_cabecera')) {
            $url = Storage::put('public/images/beneficios', $request->file('imagen_cabecera'));
            $entity['imagen_cabecera_url'] = $url;
            $entity['imagen_cabecera_src'] = str_replace('public', 'storage', $url);
        }

        $entity = Beneficio::create($entity);
        
        if ($entity->imagen_cuadro_url) {
            $entity->imagen_cuadro()->create([
                'url' => $entity->imagen_cuadro_url
            ]);
            $entity->imagen_cuadro_src = str_replace('public', 'storage', $entity->imagen_cuadro_url);
            $entity->save();
        }

        if ($entity->imagen_cabecera_url) {
            $entity->imagen_cabecera()->create([
                'url' => $entity->imagen_cabecera_url
            ]);
            $entity->imagen_cabecera_src = str_replace('public', 'storage', $entity->imagen_cabecera_url);
            $entity->save();
        }

        return redirect(Rutas::BENEFICIO_INDEX)->with("message", "Beneficio agregado!.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Beneficio  $beneficio
     * @return \Illuminate\Http\Response
     */
    public function show(Beneficio $beneficio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Beneficio  $beneficio
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $entity = Beneficio::findOrFail($id);
        $canjeables = Canjeable::pluck('nombre', 'id');

        return view('admin.beneficio.edit', [
            'user' => $user,
            'entity' => $entity,
            'canjeables' => $canjeables
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Beneficio  $beneficio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $entity = $request->except(['_token', '_method' ]);
        $activa = (isset($entity['activa']) && $entity['activa'] = 'on') ? 1 : 0;
        $entity['activa'] = $activa;
        $destacada = (isset($entity['destacada']) && $entity['destacada'] = 'on') ? 1 : 0;
        $entity['destacada'] = $destacada;
        unset($entity['imagen_cuadro']);
        unset($entity['imagen_cabecera']);
        Beneficio::where('id', '=', $id)->update($entity);

        if ($request->file('imagen_cuadro')) {
            
            $url = Storage::put('public/images/beneficios', $request->file('imagen_cuadro'));
            
            if ($entity['imagen_cuadro_url']) {
                
                $beneficio = Beneficio::findOrFail($id);
                
                Storage::delete($beneficio->imagen_cuadro->url);
                
                $beneficio->imagen_cuadro_url = $url;
                $beneficio->imagen_cuadro_src = str_replace('public', 'storage', $url);
                
                $beneficio->imagen_cuadro()->update([
                    'url' => $url
                ]);

                $beneficio->save();
            } else {
                $beneficio = Beneficio::findOrFail($id);
                $beneficio->imagen_cuadro_url = $url;
                $beneficio->imagen_cuadro_src = str_replace('public', 'storage', $url);
                
                $beneficio->imagen_cuadro()->create([
                    'url' => $url
                ]);

                $beneficio->save();
            }

        }

        if ($request->file('imagen_cabecera')) {
            
            $url = Storage::put('public/images/beneficios', $request->file('imagen_cabecera'));
            
            if ($entity['imagen_cabecera_url']) {
                
                $beneficio = Beneficio::findOrFail($id);
                
                Storage::delete($beneficio->imagen_cabecera->url);
                
                $beneficio->imagen_cabecera_url = $url;
                $beneficio->imagen_cabecera_src = str_replace('public', 'storage', $url);
                
                $beneficio->imagen_cabecera()->update([
                    'url' => $url
                ]);

                $beneficio->save();
            } else {
                $beneficio = Beneficio::findOrFail($id);

                $beneficio->imagen_cabecera_url = $url;
                $beneficio->imagen_cabecera_src = str_replace('public', 'storage', $url);
                
                $beneficio->imagen_cabecera()->create([
                    'url' => $url
                ]);

                $beneficio->save();
            }

        }

        return redirect(Rutas::BENEFICIO_INDEX)->with("message", "Beneficio actualizado!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Beneficio $beneficio
     * @return \Illuminate\Http\Response
     */
    public function deleted($id)
    {
        $entity = Beneficio::findOrFail($id);
        if ($entity) {
            Beneficio::destroy($id);
        }

        return redirect(Rutas::BENEFICIO_INDEX)->with("message", "Beneficio borrado!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Beneficio  $beneficio
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Beneficio::destroy($id);

        return redirect(Rutas::BENEFICIO_INDEX)->with("message", "Beneficio borrado!.");
    }

    /**
     * Get QR Code View.
     *
     * @param  \App\Beneficio  $beneficio
     * @return \Illuminate\Http\Beneficio
     */
    public function showQR($id)
    {
        $entity = Beneficio::findOrFail($id);

        return view('admin.beneficio.qr', [
            'entity' => $entity
        ]);
    }

    /**
     * print PDF View.
     *
     * @param  \App\Beneficio  $beneficio
     * @return \Illuminate\Http\Response
     */
    public function printPDF($id)
    {
        $entity = Beneficio::findOrFail($id);
        
        $data = [
            'entity' => $entity
        ];
        
        $pdf = \PDF::loadView('admin.beneficio.pdf', $data)->stream('archivo.pdf');
    }
}
