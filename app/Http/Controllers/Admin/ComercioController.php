<?php

namespace App\Http\Controllers\Admin;

use App\Models\Comercio;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ComercioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entities = Comercio::all();

        return view('admin.comercio.index', [
            'entities' => $entities
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.comercio.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entity = $request->except(['_token', '_method']);

        if ($request->file('imagen')) {
            $url = Storage::put('public/images/comercios', $request->file('imagen'));
            $entity['imagen_url'] = $url;
            $entity['imagen_src'] = str_replace('public', 'storage', $url);
        }

        $entity = Comercio::create($entity);
        
        if ($entity->imagen_url) {
            $entity->imagen()->create([
                'url' => $entity->imagen_url
            ]);
        }

        return redirect(Rutas::COMERCIO_INDEX)->with("message", "Comercio agregado!.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comercio  $comercio
     * @return \Illuminate\Http\Response
     */
    public function show(Comercio $comercio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comercio  $comercio
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entity = Comercio::findOrFail($id);

        return view('admin.comercio.edit', compact('entity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comercio  $comercio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $entity = $request->except(['_token', '_method']);
        unset($entity['imagen']);
        Comercio::where('id', '=', $id)->update($entity);
        
        $entity = Comercio::findOrFail($id);
        
        if ($request->file('imagen')) {
            
            $url = Storage::put('public/images/comercios', $request->file('imagen'));
            
            if ($entity->imagen_url) {

                Storage::delete($entity->imagen->url);
                
                $entity->imagen_url = $url;
                $entity->imagen_src = str_replace('public', 'storage', $url);
                
                $entity->imagen()->update([
                    'url' => $url
                ]);

                $entity->save();
            } else {

                $entity->imagen_url = $url;
                $entity->imagen_src = str_replace('public', 'storage', $url);
                
                $entity->imagen()->create([
                    'url' => $url
                ]);

                $entity->save();
            }

        }

        return redirect(Rutas::COMERCIO_INDEX)->with("message", "Comercio actualizado!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comercio $comercio
     * @return \Illuminate\Http\Response
     */
    public function deleted($id)
    {
        $entity = Comercio::findOrFail($id);
        if ($entity) {
            try {
                ($entity->imagen_url) ? Storage::delete($entity->imagen->url) : null;
                Comercio::destroy($id);
            } catch (\Illuminate\Database\QueryException $e) {
                if($e->getCode() === '23000') {
                    return redirect(Rutas::COMERCIO_INDEX)->with("messageError", "El Comercio tiene asociada una Sucursal y no se puede ser borrado!.");
                } 
            }
        }

        return redirect(Rutas::COMERCIO_INDEX)->with("message", "Comercio borrado!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comercio  $comercio
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Comercio::destroy($id);

        return redirect(Rutas::COMERCIO_INDEX)->with("message", "Comercio borrado!.");
    }
}
