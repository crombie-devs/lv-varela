<?php

namespace App\Http\Controllers\Admin;

use App\Models\Sucursal;
use App\Models\Comercio;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SucursalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->rol == "ROLE_COMERCIO") {
            $entities = Sucursal::where('comercio_id', $user->comercio_id)->get();
        } else {    
            $entities = Sucursal::all();
        }
        
        return view('admin.sucursal.index', [
            'user' => $user,
            'entities' => $entities,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $comercios = Comercio::all();

        return view('admin.sucursal.create', [
            'user' => $user,
            'comercios' => $comercios,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entity = request()->except('_token');

        unset($entity['optionsRadios']);

        Sucursal::insert($entity);

        return redirect('admin/sucursales')->with("message", "Sucursal agregada!.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sucursal  $sucursal
     * @return \Illuminate\Http\Response
     */
    public function show(Sucursal $sucursal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sucursal  $sucursal
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $entity = Sucursal::findOrFail($id);
        $comercios = Comercio::all();

        return view('admin.sucursal.edit', [
            'user' => $user,
            'entity' => $entity,
            'comercios' => $comercios,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sucursal  $sucursal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $entity = request()->except(['_token', '_method']);

        unset($entity['optionsRadios']);

        Sucursal::where('id', '=', $id)->update($entity);

        return redirect('admin/sucursales')->with("message", "Sucursal actualizada!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sucursal $sucursal
     * @return \Illuminate\Http\Response
     */
    public function deleted($id)
    {
        $entity = Sucursal::findOrFail($id);
        if ($entity) {
            Sucursal::destroy($id);
        }

        return redirect('admin/sucursales')->with("message", "Sucursal borrada!.");
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sucursal  $sucursal
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Sucursal::destroy($id);

        return redirect('admin/sucursales')->with("message", "Sucursal borrada!.");
    }
}
