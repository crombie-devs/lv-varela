<?php

namespace App\Http\Controllers\Admin;

use App\Models\Beneficio;
use App\Models\UsuarioMobile;
use App\Models\BeneficioUsuario;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

class BeneficioUsuarioController extends Controller
{

    // /**
    //  * Display a listing of the resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function index()
    // {
    //     $entities = Cupon::all();

    //     return view('admin.cupon.index', [
    //         'entities' => $entities
    //     ]);
    // }

    // /**
    //  * Show the form for creating a new resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function create()
    // {
    //     $beneficios = Beneficio::pluck('nombre', 'id');
    //     $usuarios = UsuarioMobile::pluck('nombre', 'id');

    //     return view('admin.cupon.create', [
    //         'beneficios' => $beneficios,
    //         'usuarios' => $usuarios
    //     ]);
    // }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function store(Request $request)
    // {
    //     $entity = $request->except(['_token', '_method']);
    //     unset($entity['fecha_can']);
    //     $canjeado = (isset($entity['canjeado']) && $entity['canjeado'] = 'on') ? 1 : 0;
    //     $entity['canjeado'] = $canjeado;

    //     Cupon::create($entity);
        
    //     return redirect(Rutas::CUPON_INDEX)->with("message", "Cupon agregado!.");
    // }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  \App\Cupon  $Cupon
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show(Cupon $Cupon)
    // {
    //     //
    // }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  \App\Cupon  $Cupon
    //  * @return \Illuminate\Http\Response
    //  */
    // public function edit($id)
    // {
    //     $entity = Cupon::findOrFail($id);
    //     $beneficios = Beneficio::pluck('nombre', 'id');
    //     $usuarios = UsuarioMobile::pluck('nombre', 'id');

    //     return view('admin.cupon.edit', [
    //         'entity' => $entity,
    //         'beneficios' => $beneficios,
    //         'usuarios' => $usuarios
    //     ]);
    // }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  \App\Cupon  $Cupon
    //  * @return \Illuminate\Http\Response
    //  */
    // public function update(Request $request, $id)
    // {
    //     $entity = $request->except(['_token', '_method']);
    //     unset($entity['fecha_can']);
    //     $canjeado = (isset($entity['canjeado']) && $entity['canjeado'] = 'on') ? 1 : 0;
    //     $entity['canjeado'] = $canjeado;
        
    //     Cupon::where('id', '=', $id)->update($entity);

    //     return redirect(Rutas::CUPON_INDEX)->with("message", "Cupon actualizada!.");
    // }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  \App\Cupon $cupon
    //  * @return \Illuminate\Http\Response
    //  */
    // public function deleted($id)
    // {
    //     $entity = Cupon::findOrFail($id);
    //     if ($entity) {
    //         Cupon::destroy($id);
    //     }

    //     return redirect(Rutas::CUPON_INDEX)->with("message", "Cupon borrado!.");
    // }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  \App\Cupon  $cupon
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy($id)
    // {
    //     Cupon::destroy($id);

    //     return redirect(Rutas::CUPON_INDEX)->with("message", "Cupon borrado!.");
    // }

    /**
     * Return Points Halcoins.
     *
     * @param  \App\BeneficioUsuario $beneficioUsuario
     * @return \Illuminate\Http\Response
     */
    public function halcoinsAcum($beneficio_id, $usuario_id)
    {
        $puntos = BeneficioUsuario::where('beneficio_id', $beneficio_id)->where('usuario_app_id', $usuario_id)->get();

        return response()->json(count($puntos));
    }

    /**
     * Acumulated the specified resource from storage.
     *
     * @param  \App\BeneficioUsuario $beneficioUsuario
     * @return \Illuminate\Http\Response
     */
    public function halcoins($beneficio_id, $usuario_id)
    {
        $hash = BeneficioUsuario::where('beneficio_id', $beneficio_id)->where('usuario_app_id', $usuario_id)->where('fecha_generado', Carbon::now()->format('Y-m-d'))->get();
        
        if (count($hash) > 0) {
            return redirect(Rutas::BENEFICIO_INDEX)->with("message", "Solo se puede canjear un beneficio por día!.");
        } else {
            $puntos = BeneficioUsuario::where('beneficio_id', $beneficio_id)->where('usuario_app_id', $usuario_id)->get();
            $beneficio = Beneficio::findOrFail($beneficio_id);
            
            $entity = new BeneficioUsuario();
            $entity->beneficio_id = $beneficio_id;
            $entity->usuario_app_id = $usuario_id;
            $entity->fecha_generado = Carbon::now();
            $entity->timestamps = false;
            $entity->save();

            if ($beneficio->halcoins <= count($puntos)) {
                return redirect(Rutas::BENEFICIO_INDEX)->with("message", "Ya puede obtener su cupon canjeable, recuerde que los Beneficios son acumulativos por día!.");
            } else {
                return redirect(Rutas::BENEFICIO_INDEX)->with("message", "Beneficio acumulado!.");
            }
        }
        
        // localhost:8000/admin/halcoins/redeem/1/eXZLcEElBHMasa2rVX3m4PESJpw1
    }
}
