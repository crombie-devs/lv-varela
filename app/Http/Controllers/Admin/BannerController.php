<?php

namespace App\Http\Controllers\Admin;

use App\Models\Banner;
use App\Models\Oferta;
use App\Models\Beneficio;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BannerController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['entities'] = Banner::all();

        return view('admin.banner.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ofertas = Oferta::all();
        $beneficios = Beneficio::all();

        return view('admin.banner.create', [
            'ofertas' => $ofertas,
            'beneficios' => $beneficios
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entity = request()->except('_token');

        $activo = (isset($entity['activo'])) ? 1 : 0;
        $destacado = (isset($entity['destacado'])) ? 1 : 0;
        $entity['activo'] = $activo;
        $entity['destacado'] = $destacado;
        $entity['created_at'] = new \DateTime();

        unset($entity['tipo']);

        Banner::insert($entity);

        return redirect('admin/banners')->with("message", "Banner agregado!.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banner $banner
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entity = Banner::findOrFail($id);
        $ofertas = Oferta::all();
        $beneficios = Beneficio::all();

        return view('admin.banner.edit', [
            'entity' => $entity,
            'ofertas' => $ofertas,
            'beneficios' => $beneficios
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Banner $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $entity = request()->except(['_token', '_method']);

        $activo = (isset($entity['activo'])) ? 1 : 0;
        $destacado = (isset($entity['destacado'])) ? 1 : 0;
        $entity['activo'] = $activo;
        $entity['destacado'] = $destacado;

        unset($entity['tipo']);

        Banner::where('id', '=', $id)->update($entity);

        return redirect('admin/banners')->with("message", "Banner actualizado!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Banner::destroy($id);

        return redirect('admin/banners')->with("message", "Banner borrado!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner $banner
     * @return \Illuminate\Http\Response
     */
    public function deleted($id)
    {
        $entity = Banner::findOrFail($id);
        if ($entity) {
            Banner::destroy($id);
        }

        return redirect('admin/banners')->with("message", "Banner borrado!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner $banner
     * @return \Illuminate\Http\Response
     */
    public function distinguish($id)
    {
        Banner::where('destacado', 1)->update(['destacado' => 0]);

        $entity = Banner::findOrFail($id);
        $entity->destacado = 1;
        $entity->save();

        return redirect('admin/banners')->with("message", "Banner Destacado!.");
    }
}
