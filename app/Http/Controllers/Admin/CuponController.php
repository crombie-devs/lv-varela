<?php

namespace App\Http\Controllers\Admin;

use App\Models\Cupon;
use App\Models\Beneficio;
use App\Models\UsuarioMobile;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

class CuponController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entities = Cupon::all();

        return view('admin.cupon.index', [
            'entities' => $entities
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $beneficios = Beneficio::pluck('nombre', 'id');
        $usuarios = UsuarioMobile::pluck('nombre', 'id');

        return view('admin.cupon.create', [
            'beneficios' => $beneficios,
            'usuarios' => $usuarios
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entity = $request->except(['_token', '_method']);
        unset($entity['fecha_can']);
        $canjeado = (isset($entity['canjeado']) && $entity['canjeado'] = 'on') ? 1 : 0;
        $entity['canjeado'] = $canjeado;

        Cupon::create($entity);
        
        return redirect(Rutas::CUPON_INDEX)->with("message", "Cupon agregado!.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cupon  $Cupon
     * @return \Illuminate\Http\Response
     */
    public function show(Cupon $Cupon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cupon  $Cupon
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entity = Cupon::findOrFail($id);
        $beneficios = Beneficio::pluck('nombre', 'id');
        $usuarios = UsuarioMobile::pluck('nombre', 'id');

        return view('admin.cupon.edit', [
            'entity' => $entity,
            'beneficios' => $beneficios,
            'usuarios' => $usuarios
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cupon  $Cupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $entity = $request->except(['_token', '_method']);
        unset($entity['fecha_can']);
        $canjeado = (isset($entity['canjeado']) && $entity['canjeado'] = 'on') ? 1 : 0;
        $entity['canjeado'] = $canjeado;
        
        Cupon::where('id', '=', $id)->update($entity);

        return redirect(Rutas::CUPON_INDEX)->with("message", "Cupon actualizada!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cupon $cupon
     * @return \Illuminate\Http\Response
     */
    public function deleted($id)
    {
        $entity = Cupon::findOrFail($id);
        if ($entity) {
            Cupon::destroy($id);
        }

        return redirect(Rutas::CUPON_INDEX)->with("message", "Cupon borrado!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cupon  $cupon
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cupon::destroy($id);

        return redirect(Rutas::CUPON_INDEX)->with("message", "Cupon borrado!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cupon $cupon
     * @return \Illuminate\Http\Response
     */
    public function redeem($id)
    {
        Cupon::where('id', $id)->update([
            'canjeado' => 1,
            'fecha_canje' => Carbon::now()
        ]);

        return redirect(Rutas::CUPON_INDEX)->with("message", "Cupon canjeado!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cupon $cupon
     * @return \Illuminate\Http\Response
     */
    public function redeemUser($cupon_id, $usuario_id)
    {
        Cupon::where('id', $cupon_id)->where('usuario_app_id', $usuario_id)->update([
            'canjeado' => 1,
            'fecha_canje' => Carbon::now()
        ]);

        return redirect(Rutas::CUPON_INDEX)->with("message", "Cupon canjeado!.");
    }
}
