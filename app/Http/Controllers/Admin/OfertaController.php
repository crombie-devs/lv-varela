<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Models\Oferta;
use App\Models\Categoria;
use App\Models\Sucursal;
use App\Models\Dia;
use App\Models\OfertaSucursal;
use App\Models\OfertaDia;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class OfertaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->rol == "ROLE_COMERCIO") {
            $ofertas = Oferta::all();
            $entities = $ofertas;
            foreach ($ofertas as $key => $oferta) {
                if (count($oferta->sucursales) > 0) {
                    foreach ($oferta->sucursales as $sucursal) {
                        if ($sucursal->comercio_id <> $user->comercio_id) {
                            unset($entities[$key]);
                        }
                    }
                }
            }
        } else {    
            $entities = Oferta::all();
        }

        // si hay ofertas sin sucursales las desactivo.
        foreach ($entities as $oferta) {
            if (count($oferta->sucursales) == 0) {
                $of = Oferta::findOrFail($oferta->id);
                $of->activa = 0;
                $of->save();
            }
            // return redirect(Rutas::OFERTA_INDEX);
        }

        return view('admin.oferta.index', [
            'user' => $user,
            'entities' => $entities,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $categorias = Categoria::pluck('nombre', 'id');
        $sucursales = Sucursal::pluck('nombre', 'id');
        $dias = Dia::pluck('nombre', 'id');

        return view('admin.oferta.create', [
            'user' => $user,
            'categorias' => $categorias,
            'sucursales' => $sucursales,
            'dias' => $dias
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entity = $request->except(['_token', '_method']);
        $activa = (isset($entity['activa']) && $entity['activa'] = 'on') ? 1 : 0;
        $entity['activa'] = $activa;
        $destacada = (isset($entity['destacada']) && $entity['destacada'] = 'on') ? 1 : 0;
        $entity['destacada'] = $destacada;

        if ($request->file('imagen_cuadro')) {
            $url = Storage::put('public/images/ofertas', $request->file('imagen_cuadro'));
            $entity['imagen_cuadro_url'] = $url;
            $entity['imagen_cuadro_src'] = str_replace('public', 'storage', $url);
        }

        if ($request->file('imagen_cabecera')) {
            $url = Storage::put('public/images/ofertas', $request->file('imagen_cabecera'));
            $entity['imagen_cabecera_url'] = $url;
            $entity['imagen_cabecera_src'] = str_replace('public', 'storage', $url);
        }

        $entity = Oferta::create($entity);
        
        if ($entity->imagen_cuadro_url) {
            $entity->imagen_cuadro()->create([
                'url' => $entity->imagen_cuadro_url
            ]);
            $entity->imagen_cuadro_src = str_replace('public', 'storage', $entity->imagen_cuadro_url);
            $entity->save();
        }

        if ($entity->imagen_cabecera_url) {
            $entity->imagen_cabecera()->create([
                'url' => $entity->imagen_cabecera_url
            ]);
            $entity->imagen_cabecera_src = str_replace('public', 'storage', $entity->imagen_cabecera_url);
            $entity->save();
        }

        foreach ($request->sucursales as $sucursal) {
            $ofertaSucursal = new OfertaSucursal;
            $ofertaSucursal->oferta_id = $entity->id;
            $ofertaSucursal->sucursal_id = $sucursal;
            $ofertaSucursal->timestamps = false;
            $ofertaSucursal->save();
        }

        foreach ($request->dias as $dia) {
            $ofertaDia = new OfertaDia;
            $ofertaDia->oferta_id = $entity->id;
            $ofertaDia->dia_id = $dia;
            $ofertaDia->timestamps = false;
            $ofertaDia->save();
        }

        return redirect(Rutas::OFERTA_INDEX)->with("message", "Oferta agregada!.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Oferta  $oferta
     * @return \Illuminate\Http\Response
     */
    public function show(Oferta $oferta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Oferta  $oferta
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $entity = Oferta::findOrFail($id);
        $categorias = Categoria::pluck('nombre', 'id');
        $sucursales = Sucursal::pluck('nombre', 'id');
        $dias = Dia::pluck('nombre', 'id');

        return view('admin.oferta.edit', [
            'user' => $user,
            'entity' => $entity,
            'categorias' => $categorias,
            'sucursales' => $sucursales,
            'dias' => $dias
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Oferta  $oferta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $entity = $request->except(['_token', '_method' ]);
        $activa = (isset($entity['activa']) && $entity['activa'] = 'on') ? 1 : 0;
        $entity['activa'] = $activa;
        $destacada = (isset($entity['destacada']) && $entity['destacada'] = 'on') ? 1 : 0;
        $entity['destacada'] = $destacada;
        unset($entity['sucursales']);
        unset($entity['dias']);
        unset($entity['fecha_des']);
        unset($entity['fecha_has']);
        unset($entity['imagen_cuadro']);
        unset($entity['imagen_cabecera']);
        Oferta::where('id', '=', $id)->update($entity);

        $entity = Oferta::findOrFail($id);
        
        if ($request->file('imagen_cuadro')) {
            
            $url = Storage::put('public/images/ofertas', $request->file('imagen_cuadro'));
            
            if ($entity->imagen_cuadro_url) {

                Storage::delete($entity->imagen_cuadro->url);
                
                $entity->imagen_cuadro_url = $url;
                $entity->imagen_cuadro_src = str_replace('public', 'storage', $url);
                
                $entity->imagen_cuadro()->update([
                    'url' => $url
                ]);

                $entity->save();
            } else {

                $entity->imagen_cuadro_url = $url;
                $entity->imagen_cuadro_src = str_replace('public', 'storage', $url);
                
                $entity->imagen_cuadro()->create([
                    'url' => $url
                ]);

                $entity->save();
            }
        }

        if ($request->file('imagen_cabecera')) {
            
            $url = Storage::put('public/images/ofertas', $request->file('imagen_cabecera'));
            
            if ($entity->imagen_cabecera_url) {

                Storage::delete($entity->imagen_cabecera->url);
                
                $entity->imagen_cabecera_url = $url;
                $entity->imagen_cabecera_src = str_replace('public', 'storage', $url);
                
                $entity->imagen_cabecera()->update([
                    'url' => $url
                ]);

                $entity->save();
            } else {

                $entity->imagen_cabecera_url = $url;
                $entity->imagen_cabecera_src = str_replace('public', 'storage', $url);
                
                $entity->imagen_cabecera()->create([
                    'url' => $url
                ]);

                $entity->save();
            }
        }

        OfertaSucursal::where('oferta_id', $id)->delete();
        OfertaDia::where('oferta_id', $id)->delete();

        foreach ($request->sucursales as $sucursal) {
            $ofertaSucursal = new OfertaSucursal;
            $ofertaSucursal->oferta_id = $id;
            $ofertaSucursal->sucursal_id = $sucursal;
            $ofertaSucursal->timestamps = false;
            $ofertaSucursal->save();
        }

        foreach ($request->dias as $dia) {
            $ofertaDia = new OfertaDia;
            $ofertaDia->oferta_id = $id;
            $ofertaDia->dia_id = $dia;
            $ofertaDia->timestamps = false;
            $ofertaDia->save();
        }

        return redirect(Rutas::OFERTA_INDEX)->with("message", "Oferta actualizada!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Oferta $oferta
     * @return \Illuminate\Http\Response
     */
    public function deleted($id)
    {
        $entity = Oferta::findOrFail($id);
        if ($entity) {
            ($entity->imagen_cuadro_url) ? Storage::delete($entity->imagen_cuadro->url) : null;
            ($entity->imagen_cabecera_url) ? Storage::delete($entity->imagen_cabecera->url) : null;
            Oferta::destroy($id);
        }

        return redirect(Rutas::OFERTA_INDEX)->with("message", "Oferta borrada!.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Oferta  $oferta
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Oferta::destroy($id);

        return redirect(Rutas::OFERTA_INDEX)->with("message", "Oferta borrada!.");
    }

    /**
     * Get QR Code View.
     *
     * @param  \App\Oferta  $oferta
     * @return \Illuminate\Http\Oferta
     */
    public function showQR($id)
    {
        $entity = Oferta::findOrFail($id);

        return view('admin.oferta.qr', [
            'entity' => $entity
        ]);
    }

    /**
     * print PDF View.
     *
     * @param  \App\Oferta  $beneficio
     * @return \Illuminate\Http\Response
     */
    public function printPDF($id)
    {
        $entity = Oferta::findOrFail($id);
        
        $data = [
            'entity' => $entity
        ];
        
        $pdf = \PDF::loadView('admin.oferta.pdf', $data)->stream('archivo.pdf');
    }
}
