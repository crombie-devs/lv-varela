<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Contacto;
use Illuminate\Http\Request;
use App\Http\Requests\V1\ContactoRequest;
use Illuminate\Support\Facades\Auth;

class ContactoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\V1\ContactoRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactoRequest $request)
    {
        $request->validated();

        // $user = Auth::user();

        $entity = new Contacto();
        $entity->email = $request->input('email');
        $entity->motivo = $request->input('motivo');
        $entity->mensaje = $request->input('mensaje');

        $res = $entity->save();

        if ($res) {
            return response()->json(['message' => 'Entity create succesfully'], 201);
        }
        return response()->json(['message' => 'Error to create entity'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contacto  $contacto
     * @return \Illuminate\Http\Response
     */
    public function show(Contacto $contacto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contacto  $contacto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contacto $contacto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contacto  $contacto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contacto $contacto)
    {
        //
    }
}
