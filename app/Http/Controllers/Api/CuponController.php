<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\V1\CuponRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\CuponResource;
use App\Models\Cupon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class CuponController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CuponResource::collection(Cupon::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\V1\CuponRequest $request
     * @param  \App\Models\Cupon $cupon
     * @return \Illuminate\Http\Response
     */
    public function store(CuponRequest $request) 
    {
        $request->validated();

        $entity = new Cupon();
        $entity->beneficio_id = $request->input('beneficio_id');
        $entity->usuario_app_id = $request->input('usuario_id');
        $entity->canjeado = 0;

        $cupones = Cupon::where('beneficio_id', $entity->beneficio_id)->where('usuario_app_id', $entity->usuario_app_id)->get();
        
        if (count($cupones) > 0) {
            $flag_crear = true;
            foreach ($cupones as $cupon) {
                if (Carbon::parse($cupon->created_at)->locale('es')->isoFormat('LL') == Carbon::parse(Carbon::now())->locale('es')->isoFormat('LL')) {
                    $flag_crear = false;
                }
            }
            
            if ($flag_crear) {
                $res = $entity->save();
        
                if ($res) {
                    return response()->json(['message' => 'Cupon generado.'], 201);
                } else {
                    return response()->json(['message' => 'No se pudo generar el cupon.'], 500);
                }    
            } else {
                return response()->json(['message' => 'Ya ha sido generado un cupon para este beneficio y todavia no se canjeo.'], 500);
            }
            
        } else {
            $res = $entity->save();
    
            if ($res) {
                return response()->json(['message' => 'Cupon generado.'], 201);
            } else {
                return response()->json(['message' => 'No se pudo generar el cupon.'], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cupon $cupon
     * @return \Illuminate\Http\Response
     */
    public function show($usuario_id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\V1\CuponRequest $request
     * @param  \App\Models\Cupon $cupon
     * @return \Illuminate\Http\Response
     */
    public function update(CuponRequest $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cupon  $cupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cupon $cupon)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cupon $cupon
     * @return \Illuminate\Http\Response
     */
    public function redeem($id)
    {
        Cupon::where('id', $id)->update([
            'canjeado' => 1,
            'fecha_canje' => Carbon::now()
        ]);

        return response()->json(['message' => 'Cupon canjeado.'], 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cupon $cupon
     * @return \Illuminate\Http\Response
     */
    public function canjear(Request $request)
    {
        $cupon_id = $request->input('cupon_id');
        $usuario_id = $request->input('usuario_id');

        Cupon::where('id', $cupon_id)->where('usuario_app_id', $usuario_id)->update([
            'canjeado' => 1,
            'fecha_canje' => Carbon::now()
        ]);

        return response()->json(['message' => 'Cupon canjeado.'], 201);
    }
}
