<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Oferta;
use App\Models\OfertaUsuario;
use App\Models\OfertaDia;
use Illuminate\Http\Request;
use App\Http\Requests\V1\OfertaUsuarioRequest;
use App\Http\Resources\OfertaUsuarioResource;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class OfertaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return OfertaUsuarioResource::collection(OfertaUsuario::all());
    }

    /**
     * Return Points Halcoins.
     *
     * @param  \App\OfertaUsuario $ofertaUsuario
     * @return \Illuminate\Http\Response
     */
    public function halcoinsAcum($oferta_id, $usuario_id)
    {
        $puntos = OfertaUsuario::where('oferta_id', $oferta_id)->where('usuario_app_id', $usuario_id)->get();

        return response()->json(count($puntos));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\V1\OfertaUsuarioRequest $request
     * @param  \App\Models\Oferta $oferta
     * @return \Illuminate\Http\Response
     */
    public function store(OfertaUsuarioRequest $request) {
    // public function store(OfertaUsuarioRequest $request, $oferta_id) {
        $request->validated();

        $usuario_id = $request->input('usuario_id');
        $oferta_id = $request->input('oferta_id');
        
        $hash = OfertaUsuario::where('oferta_id', $oferta_id)->where('usuario_app_id', $usuario_id)->where('fecha_generado', Carbon::now()->format('Y-m-d'))->get();
        
        if (count($hash) > 0) {
            return response()->json(['message' => 'Solo se puede canjear un oferta por día!.'], 500);
        } else {

            $oferta_dias = OfertaDia::where('oferta_id', $oferta_id)->get();

            $dia_hoy = Carbon::now()->dayOfWeek;
            
            $flag = false;
            foreach ($oferta_dias as $key => $dia) {
                if ($dia->dia_id == $dia_hoy) {
                    $flag = true;
                }
            }
            
            if ($flag) {

                $puntos = OfertaUsuario::where('oferta_id', $oferta_id)->where('usuario_app_id', $usuario_id)->get();
                $oferta = Oferta::findOrFail($oferta_id);
                
                $entity = new OfertaUsuario();
                $entity->oferta_id = $oferta_id;
                $entity->usuario_app_id = $usuario_id;
                $entity->fecha_generado = Carbon::now();
                $entity->timestamps = false;
                $entity->save();
    
                return response()->json(['message' => 'Oferta canjeada!.'], 201);

            } else {
                return response()->json(['message' => 'Esta oferta no se puede canjear hoy!.'], 500);
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Oferta $oferta
     * @return \Illuminate\Http\Response
     */
    public function show($usuario_id)
    {
        return OfertaUsuarioResource::collection(OfertaUsuario::where('usuario_app_id', $usuario_id)->get());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\V1\OfertaRequest $request
     * @param  \App\Models\Oferta $oferta
     * @return \Illuminate\Http\Response
     */
    public function update(OfertaRequest $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Oferta  $oferta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Oferta $oferta)
    {
        //
    }
}
