<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Beneficio;
use App\Models\BeneficioUsuario;
use Illuminate\Http\Request;
use App\Http\Requests\V1\BeneficioUsuarioRequest;
use App\Http\Resources\BeneficioUsuarioResource;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class BeneficioController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return BeneficioUsuarioResource::collection(BeneficioUsuario::all());
    }

    /**
     * Return Points Halcoins.
     *
     * @param  \App\BeneficioUsuario $beneficioUsuario
     * @return \Illuminate\Http\Response
     */
    public function halcoinsAcum($beneficio_id, $usuario_id)
    {
        $puntos = BeneficioUsuario::where('beneficio_id', $beneficio_id)->where('usuario_app_id', $usuario_id)->get();

        return response()->json(count($puntos));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\V1\BeneficioUsuarioRequest $request
     * @param  \App\Models\Beneficio $beneficio
     * @return \Illuminate\Http\Response
     */
    public function store(BeneficioUsuarioRequest $request) {
    // public function store(BeneficioUsuarioRequest $request, $beneficio_id) {
        $request->validated();

        $usuario_id = $request->input('usuario_id');
        $beneficio_id = $request->input('beneficio_id');
        
        $hash = BeneficioUsuario::where('beneficio_id', $beneficio_id)->where('usuario_app_id', $usuario_id)->where('fecha_generado', Carbon::now()->format('Y-m-d'))->get();
        
        if (count($hash) > 0) {
            return response()->json(['message' => 'Solo se puede canjear un beneficio por día!.'], 500);
        } else {
            $puntos = BeneficioUsuario::where('beneficio_id', $beneficio_id)->where('usuario_app_id', $usuario_id)->get();
            $beneficio = Beneficio::findOrFail($beneficio_id);
            
            $entity = new BeneficioUsuario();
            $entity->beneficio_id = $beneficio_id;
            $entity->usuario_app_id = $usuario_id;
            $entity->fecha_generado = Carbon::now();
            $entity->timestamps = false;
            $entity->save();

            if ($beneficio->halcoins <= count($puntos)) {
                return response()->json(['message' => 'Ya puede obtener su cupon canjeable, recuerde que los Beneficios son acumulativos por día!.'], 201);
            } else {
                return response()->json(['message' => 'Beneficio acumulado!.'], 201);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Beneficio $beneficio
     * @return \Illuminate\Http\Response
     */
    public function show($usuario_id)
    {
        return BeneficioUsuarioResource::collection(BeneficioUsuario::where('usuario_app_id', $usuario_id)->get());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\V1\BeneficioRequest $request
     * @param  \App\Models\Beneficio $beneficio
     * @return \Illuminate\Http\Response
     */
    public function update(BeneficioRequest $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Beneficio  $beneficio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Beneficio $beneficio)
    {
        //
    }
}
