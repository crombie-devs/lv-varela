<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\UsuarioMobile;
use Illuminate\Http\Request;
use App\Http\Requests\V1\UsuarioMobileRequest;
use App\Http\Resources\UsuarioMobileResource;
use Illuminate\Support\Facades\Auth;

class UsuarioMobileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return UsuarioMobileResource::collection(UsuarioMobile::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\V1\UsuarioMobileRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsuarioMobileRequest $request)
    {
        $request->validated();

        $entity = new UsuarioMobile();

        $entity->id = $request->input('id');
        $entity->email = $request->input('email');
        $entity->nombre = $request->input('nombre');
        $entity->apellido = $request->input('apellido');
        $entity->fecha_nacimiento = $request->input('fecha_nacimiento');
        $entity->device_key = $request->input('device_key');
        $entity->barrio = $request->input('barrio');
        $entity->rol = 'ROLE_APP';

        $res = $entity->save();

        if ($res) {
            return response()->json(['message' => 'Entity create succesfully'], 201);
        }
        return response()->json(['message' => 'Error to create entity'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UsuarioMobile $usuarioMobile
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new UsuarioMobileResource(UsuarioMobile::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\V1\UsuarioMobileRequest $request
     * @param  \App\Models\UsuarioMobile $usuarioMobile
     * @return \Illuminate\Http\Response
     */
    public function update(UsuarioMobileRequest $request, $id)
    {
        $entity = UsuarioMobile::findOrFail($id);

        $entity->email = $request->input('email');
        $entity->nombre = $request->input('nombre');
        $entity->apellido = $request->input('apellido');
        $entity->fecha_nacimiento = $request->input('fecha_nacimiento');
        $entity->device_key = $request->input('device_key');
        $entity->barrio = $request->input('barrio');

        $res = $entity->save();

        if ($res) {
            return response()->json(['message' => 'Entity save succesfully'], 201);
        }
        return response()->json(['message' => 'Error to save entity'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UsuarioMobile $usuarioMobile
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsuarioMobile $usuarioMobile)
    {
        //
    }
}
