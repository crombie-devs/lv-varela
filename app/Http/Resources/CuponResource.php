<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\BeneficioResource;
use App\Http\Resources\UsuarioMobileResource;

class CuponResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'beneficio' => new BeneficioResource($this->beneficio),
            'usuario' => new UsuarioMobileResource($this->usuario),
            'canjeado' => $this->canjeado,
            'fecha_canje' => (string) $this->fecha_canje,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}