<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\BeneficioResource;
use App\Http\Resources\UsuarioMobileResource;

class BeneficioUsuarioResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'beneficio_id' => $this->beneficio_id,
            'beneficio' => new BeneficioResource($this->beneficio),
            'usuario_app_id' => $this->usuario_app_id,
            'usuario' => new UsuarioMobileResource($this->usuario),
            'fecha_generado' => (string) $this->fecha_generado,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}