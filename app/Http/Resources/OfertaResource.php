<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\CategoriaResource;
use App\Http\Resources\SucursalResource;
use App\Http\Resources\DiaResource;

class OfertaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nombre' => $this->nombre,
            'descripcion' => $this->descripcion,
            'descripcion_corta' => $this->descripcion_corta,
            'ganancia' => $this->ganancia, 
            'categoria' => new CategoriaResource($this->categoria),
            'sucursales' => SucursalResource::collection($this->sucursales),
            'imagen_cuadro_src' => $this->imagen_cuadro_src,
            'imagen_cabecera_src' => $this->imagen_cabecera_src,
            'destacada' => $this->destacada,
            'activa' => $this->activa,
            'dias' => DiaResource::collection($this->dias),
            'fecha_desde' => (string) $this->fecha_desde,
            'fecha_hasta' => (string) $this->fecha_hasta,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}