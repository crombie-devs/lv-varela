<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\OfertaResource;
use App\Http\Resources\BeneficioResource;

class BannerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'activo' => $this->activo,
            'destacado' => $this->destacado,
            'oferta' => new OfertaResource($this->oferta),
            'beneficio' => new BeneficioResource($this->beneficio),
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
