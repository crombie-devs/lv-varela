<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\OfertaResource;
use App\Http\Resources\UsuarioMobileResource;

class OfertaUsuarioResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'oferta_id' => $this->oferta_id,
            'oferta' => new OfertaResource($this->oferta),
            'usuario_app_id' => $this->usuario_app_id,
            'usuario' => new UsuarioMobileResource($this->usuario),
            'fecha_generado' => (string) $this->fecha_generado,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}