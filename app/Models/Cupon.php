<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cupon extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cupones';

    protected $fillable = [
        'id',
        'beneficio_id',
        'usuario_app_id',
        'canjeado',
        'fecha_canje'
    ];

    public $timestamp = true;

    protected $hidden = [
        'updated_at',
        'created_at'
    ];

    public function beneficio()
    {
        return $this->belongsTo(Beneficio::class, 'beneficio_id');
    }

    public function usuario()
    {
        return $this->belongsTo(UsuarioMobile::class, 'usuario_app_id');
    }
}
