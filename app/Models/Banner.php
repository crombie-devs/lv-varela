<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $fillable = [
        'activo',
        'oferta_id',
        'beneficio_id',
        'created_at',
        'updated_at'
    ];

    public $timestamp = true;

    protected $hidden = [
        'updated_at',
        'created_at'
    ];

    public function beneficio()
    {
        return $this->belongsTo(Beneficio::class, 'beneficio_id');
    }

    public function oferta()
    {
        return $this->belongsTo(Oferta::class, 'oferta_id');
    }
}
