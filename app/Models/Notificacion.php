<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notificacion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notificaciones';

    public function usuario()
    {
        return $this->belongsTo(UsuarioMobile::class, 'usuario_id');
    }
}
