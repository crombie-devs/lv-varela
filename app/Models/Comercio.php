<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comercio extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'nombre',
        'descripcion',
        'email',
        'telefono',
        'web',
        'imagen_url',
        'created_at',
        'updated_at',
    ];

    public function sucursales(){
        return $this->hasMany(Sucursal::class);
    }

    public function imagen()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}
