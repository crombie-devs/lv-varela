<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contacto extends Model
{
    use HasFactory;

    protected $fillable = [
        'email',
        'motivo',
        'mensaje'
    ];

    public $timestamp = true;

    protected $hidden = [
        'updated_at',
        'created_at'
    ];
}
