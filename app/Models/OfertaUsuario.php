<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OfertaUsuario extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ofertas_usuarios';

    public $timestamp = false;
    
    protected $fillable = [
        'oferta_id',
        'usuario_app_id',
        'fecha_generado'
    ];

    protected $hidden = [
        'updated_at',
        'created_at'
    ];

    public function oferta()
    {
        return $this->belongsTo(Oferta::class, 'oferta_id');
    }

    public function usuario()
    {
        return $this->belongsTo(UsuarioMobile::class, 'usuario_app_id');
    }
}