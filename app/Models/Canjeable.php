<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Canjeable extends Model
{
    protected $fillable = [
        'nombre'
    ];

    public $timestamp = true;

    protected $hidden = [
        'updated_at',
        'created_at'
    ];

    public function sucursales(){
        return $this->belongsToMany(Sucursal::class, 'canjeables_sucursales', 'canjeable_id', 'sucursal_id');
    }
}
