<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Oferta extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'nombre',
        'descripcion',
        'descripcion_corta',
        'ganancia',
        'destacada',
        'activa',
        'fecha_desde',
        'fecha_hasta',
        'categoria_id',
        'imagen_cuadro_url',
        'imagen_cabecera_url',
        'created_at',
        'updated_at',
    ];

    public function categoria()
    {
        return $this->belongsTo(Categoria::class, 'categoria_id');
    }

    public function sucursales(){
        return $this->belongsToMany(Sucursal::class, 'ofertas_sucursales', 'oferta_id', 'sucursal_id');
    }

    public function dias(){
        return $this->belongsToMany(Dia::class, 'ofertas_dias', 'oferta_id', 'dia_id');
    }

    public function imagen_cuadro()
    {
        return $this->morphOne(ImagenCuadro::class, 'imageable');
    }

    public function imagen_cabecera()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}
