<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sucursales';

    protected $fillable = [
        'nombre',
        'direccion',
        'telefono',
        'lat',
        'long',
        'comercio_id'
    ];

    public $timestamp = true;

    protected $hidden = [
        'updated_at',
        'created_at'
    ];

    public function comercio()
    {
        return $this->belongsTo(Comercio::class, 'comercio_id');
    }
}
