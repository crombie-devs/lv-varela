<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Beneficio extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'nombre',
        'descripcion',
        'descripcion_corta',
        'ganancia',
        'alcance',
        'destacada',
        'activa',
        'fecha_desde',
        'fecha_hasta',
        'canjeable_id',
        'halcoins',
        'imagen_cuadro_url',
        'imagen_cabecera_url',
        'created_at',
        'updated_at',
    ];

    public function canjeable()
    {
        return $this->belongsTo(Canjeable::class, 'canjeable_id');
    }

    public function imagen_cuadro()
    {
        return $this->morphOne(ImagenCuadro::class, 'imageable');
    }

    public function imagen_cabecera()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}
