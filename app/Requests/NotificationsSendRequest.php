<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class NotificationsSendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'description_text' => 'required|string',
            'link' => 'nullable|string|url',
            'image' => 'nullable|file|mimes:png,jpeg,jpg',

        ];
    }

    public function attributes()
    {
        return [
            'title' => trans('fields.term_and_condition.text'),
            'description_text' => trans('fields.term_and_condition.text'),
//            'link' => trans('fields.term_and_condition.text'),

        ];
    }
}
