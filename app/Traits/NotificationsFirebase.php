<?php
namespace App\Traits;
use App\Entities\NotificationUserSend;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use GuzzleHttp\Client;
use App\OAuth\SocialUserProviderInterface;
use Laravel\Socialite\Facades\Socialite;


trait NotificationsFirebase{

    public function sendNotification($title=null,$description=null,$image = null ,$data = null ,$tokens=null, $users=null){

        $notification = Notification::fromArray([
            'title' => $title,
            'body' => $description,
            'image'=> $image
        ]);
        $messaging = app('firebase.messaging');
        $messageAll = [];

        foreach ($tokens as $token){
            $messageAll[] = CloudMessage::withTarget('token', $token)
            ->withNotification($notification)
            ->withData($data);
        }

        foreach ($users as $user){
            NotificationUserSend::create(['user_id'=>$user,
                'name'=>$title,
                'description'=>$description,
                'read'=>0,
                'data'=>json_encode($data),
                ]);
        }
        if(!empty($tokens)){
            $messaging->sendAll($messageAll);
        }

    }

}