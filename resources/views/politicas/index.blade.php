<!doctype html>
<html lang="en" class="h-100">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
        <meta name="generator" content="Hugo 0.88.1">
        <title>Sticky Footer Template · Bootstrap v5.1</title>

        <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/sticky-footer/">

        

        <!-- Bootstrap core CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>

        <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
            font-size: 3.5rem;
            }
        }

        /* Custom page CSS
        -------------------------------------------------- */
        /* Not required for template or sticky footer method. */

        .container {
            width: auto;
            max-width: 680px;
            padding: 0 15px;
        }

        </style>

        
        <!-- Custom styles for this template -->
        <link href="sticky-footer.css" rel="stylesheet">
    </head>
    <body class="d-flex flex-column h-100">
        
        <!-- Begin page content -->
        <main class="flex-shrink-0">
            <div class="container">
                <h1 class="mt-5">Politicas de Privacidad</h1>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">1 - A través de esta aplicación no se accede a las cuentas de correo de los usuarios.</li>
                    <li class="list-group-item">2 - La aplicación no guarda datos ni hace seguimientos sobre tiempos y horarios de utilización.</li>
                    <li class="list-group-item">3 - La aplicación no guarda información relativa a tu dispositivo como, por ejemplo, fallos, actividad del sistema, ajustes del hardware, tipo de navegador, idioma del navegador.</li>
                    <li class="list-group-item">4 - La aplicación no accede a tus contactos ni agendas.</li>
                    <li class="list-group-item">5 - La aplicación no recopila información sobre tu ubicación real.</li>
                    <li class="list-group-item">6 - Remarketing con Google AdMob Proveedores como Google, utilizan cookies de primer nivel y cookies de terceros u otros identificadores de terceros para compilar datos sobre las interacciones de los usuarios con las impresiones de anuncios y otras funciones de servicio de anuncios.</li>
                    <li class="list-group-item">7 - Clasificación por edades: <br>Solamente apto para mayores de edad.</li>
                    <li class="list-group-item">8 - Varela mas Cerca, no Google, es el único responsable de Varela mas Cerca y su contenido.</li>
                    <li class="list-group-item">9 - Mantenimiento y Soporte: <br>Varela mas Cerca, no Google, estará obligado a proporcionar dicho mantenimiento o soporte.</li>
                    <li class="list-group-item">10 - Cargos y cuotas: <br>Cualquier uso de esta aplicación es totalmente gratuito.</li>
                    <li class="list-group-item">11 - Cambios en nuestra Política de Privacidad: <br>Nuestra Política de Privacidad puede cambiar de vez en cuando. <br>Publicaremos cualquier cambio de política de privacidad en esta página, por lo que debe revisarla periódicamente</li>
                </ul>
            </div>
        </main>

        <footer class="footer mt-auto py-3 bg-light">
            <div class="container">
                <span class="text-muted">Varela + Cerca.</span>
            </div>
        </footer>

    </body>
</html>