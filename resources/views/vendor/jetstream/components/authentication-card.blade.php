<div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0">
    <div>
        {{ $logo }}
    </div>
    
    <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
        {{ $slot }}
    </div>
</div>

<style>

    body {

        background: linear-gradient(to bottom right, #5e9f98, #236655) !important;

    }

    .login-box-body {

        border-radius: 8px;

    }

</style>
