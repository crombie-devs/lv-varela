@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(Session::has('message'))
    <div class="alert alert-success">
        {{ Session::get('message') }}
    </div>
@endif

@if(Session::has('messageError'))
    <div class="alert alert-danger">
        {{ Session::get('messageError') }}
    </div>
@endif

{{-- <button onclick="startFCM()"
    class="btn btn-danger btn-flat">Allow notification
</button> --}}

<div class="card-body">
    <div class="form-group">
        <label for="usuario_id">Selecciona Usuario</label>
        <select class="form-control" id="usuario_id" name="usuario_id" placeholder="Seleccione Usuario">
            <option value="todos">Todos</option>
            @foreach ($usuariosMobile as $key => $usuario)    
                <option @if (isset($entity) && $usuario->id == $entity->usuario_id) selected @endif value="{{ $usuario->id }}">{{ $usuario->email }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="mensaje">Mensaje</label>
        <input type="text" class="form-control" id="mensaje" name="mensaje" placeholder="Escribe un Mensaje" value="{{ (isset($entity->mensaje)) ? $entity->mensaje : '' }}">
    </div>
    {{-- <div class="form-group">
        <label for="cuerpo">Cuerpo</label>
        <textarea type="text" class="form-control" id="cuerpo" name="cuerpo" placeholder="Escribe un Cuerpo" value="{{ (isset($entity->cuerpo)) ? $entity->cuerpo : '' }}"></textarea>
    </div>
    <div class="row">
        <div class="col-4">
            <div class="form-group">
                <label for="imagen">Imagen Adjunta</label>
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="imagen">
                        <label class="custom-file-label" for="imagen">Buscar imagen local</label>
                    </div>
                    <div class="input-group-append">
                        <span class="input-group-text">Subir</span>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
</div>

<div class="card-footer">
    <button type="submit" onclick="return confirm('¿Seguro quieres enviar este Mensaje?')" class="btn btn-info pull-right"><i class="far fa-comment-dots"></i> Enviar</button>
</div>

@section('js')
    <!-- The core Firebase JS SDK is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/8.3.2/firebase.js"></script>
    <script> 
        var firebaseConfig = {
            apiKey: "AIzaSyC0PUT9LW7TrAevSfopTX0ZIKjPvb1zaOk",
            authDomain: "pushnotifications-55975.firebaseapp.com",
            projectId: "pushnotifications-55975",
            storageBucket: "pushnotifications-55975.appspot.com",
            messagingSenderId: "1068937041280",
            appId: "1:1068937041280:web:cbbc06a0d4247696f45d65",
            measurementId: "G-8TH95Q91QG"
        };

        firebase.initializeApp(firebaseConfig);
        const messaging = firebase.messaging();

        function startFCM() {
            messaging
                .requestPermission()
                .then(function () {
                    return messaging.getToken()
                })
                .then(function (response) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: '{{ route("notificaciones.storeTokenBrowser") }}',
                        type: 'POST',
                        data: {
                            token: response
                        },
                        dataType: 'JSON',
                        success: function (response) {
                            alert('Token stored.');
                        },
                        error: function (error) {
                            alert(error);
                        },
                    });

                }).catch(function (error) {
                    alert(error);
                })
            ;
        }

        messaging.onMessage(function (payload) {
            const title = payload.notification.title;
            const options = {
                body: payload.notification.body,
                icon: payload.notification.icon,
            };
            new Notification(title, options);
        });

        $(function () {
            
        });
    </script>
@stop