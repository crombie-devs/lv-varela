@extends('adminlte::page')

@section('title', 'Notificaciones')

@section('content_header')
    <h1>Notificaciones</h1>
@stop

@section('content')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Nuevo</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form class="form-horizontal" action="{{ url('admin/notificaciones/') }}" method="POST" enctype="multipart/form-data">
        @csrf
            @include('admin.notificacion.form')
        </form>
    </div>           
@stop

@section('css')
    
@stop

@section('js')

@stop