@extends('adminlte::page')

@section('title', 'Notificaciones')

@section('content_header')
    <h1>Notificaciones</h1>
@stop

@section('content')

    @if(Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
    @endif

    @if(Session::has('messageError'))
        <div class="alert alert-danger">
            {{ Session::get('messageError') }}
        </div>
    @endif

    <div class="card card-outline card-info">
        <div class="card-header">
            <h3 class="card-title">Listado</h3>
            <div class="card-tools">
                <a href="{{ url('admin/notificaciones/create') }}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Nueva Notificación</a>
            </div>
        </div>
        <div class="card-body">
            <table id="notificaciones" class="table table-condensed table-hover">
                <thead>
                    <tr>
                        <th style="width: 20px">Id</th>
                        <th>Usuario</th>
                        <th>Mensaje</th>
                        <th>Fecha</th>
                        <th style="width: 40px">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($entities as $entity)
                        <tr>
                            <td>{{ $entity->id }}</td>
                            <td>{{ $entity->usuario->email }}</td>
                            <td>{{ $entity->mensaje }}</td>
                            <td>{{ \Carbon\Carbon::parse($entity->fecha)->locale('es')->isoFormat('LL') }}</td>
                            <td>
                                <div class="btn-group">
                                    <a type="button" href="{{ url('admin/notificaciones/' . $entity->id . '/edit') }}" class="btn btn-info">
                                      <i class="fas fa-search"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            
        </div>
    </div>
@stop

@section('css')
    
@stop

@section('js')
    <script> 
        $(function () {
            $("#notificaciones").DataTable({
                paging: true,
                language: {
                    url: "{{ asset('/vendor/datatables/locale/es-ar.json') }}",
                }
            });
        });
    </script>
@stop