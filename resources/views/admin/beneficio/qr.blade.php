@extends('adminlte::page')

@section('title', 'Beneficios')

@section('content_header')
    <h1>Beneficios</h1>
@stop

@section('content')

    @if(Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
    @endif

    <div class="card card-outline card-info">
        <div class="card-header">
            <h3 class="card-title">Código QR de Beneficio</h3>
        </div>
        <div class="card-body">
            <p><strong>Título: </strong>{{ $entity->nombre }}</p>
            <p><strong>Descripción: </strong>{{ $entity->descripcion }}</p>
            <p><strong>Fecha Desde: </strong>{{ \Carbon\Carbon::parse($entity->fecha_desde)->locale('es')->isoFormat('LL') }}</p>
            <p><strong>Fecha Hasta: </strong>{{ \Carbon\Carbon::parse($entity->fecha_hasta)->locale('es')->isoFormat('LL') }}</p>
            {{-- <p><strong>Sucursales: </strong>
                @if (isset($entity->sucursales) && count($entity->sucursales) <> 0)
                    @foreach($entity->sucursales as $sucursal)
                        - {{ $sucursal->nombre }}<br>
                    @endforeach
                @else
                    No tiene.
                @endif
            </p> --}}
            {{-- {!! QrCode::size(300)->generate('https://varelav2.php74.projectsunderdev.com/api/beneficios-qr/canjear/' . $entity->id) !!} --}}
            {!! QrCode::size(300)->generate('beneficio-' . $entity->id) !!}
        </div>
        <div class="card-footer">
            <a href="{{ route('beneficios.printPDF', $entity->id) }}" class="btn btn-danger pull-right"><i class="fa fa-file-pdf"></i> Generar PDF</a>
        </div>
    </div>
@stop

@section('js')
    <script> 
        $(function () {
            
        });
    </script>
@stop