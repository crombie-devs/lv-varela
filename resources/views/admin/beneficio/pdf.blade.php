<html>
    <head>
        <style>
            @page {
                margin: 0cm 0cm;
                font-family: Arial;
            }

            body {
                margin: 3cm 2cm 2cm;
            }

            header {
                position: fixed;
                top: 0cm;
                left: 0cm;
                right: 0cm;
                height: 2cm;
                background-color: #2a0927;
                color: white;
                text-align: center;
                line-height: 30px;
            }

            footer {
                position: fixed;
                bottom: 0cm;
                left: 0cm;
                right: 0cm;
                height: 2cm;
                background-color: #2a0927;
                color: white;
                text-align: center;
                line-height: 35px;
            }

            .page-break {
                page-break-after: always;
            }

            .fixed-top {
                margin-top: 10%;
            }

            .qr-code {
                text-align: center;
            }

            p {
                margin-left: 10%;
            }
        </style>
    </head>
    <body>
        <header>
            <h1>Varela + Cerca</h1>
        </header>

        <main>
            <br>
            <div class="fixed-top"></div>
            <p><strong>Título: </strong>{{ $entity->nombre }}</p>
            <p><strong>Descripción: </strong>{{ $entity->descripcion }}</p>
            <p><strong>Fecha Promoción Desde: </strong>{{ \Carbon\Carbon::parse($entity->fecha_desde)->locale('es')->isoFormat('LL') }}
            <strong>Hasta: </strong>{{ \Carbon\Carbon::parse($entity->fecha_hasta)->locale('es')->isoFormat('LL') }}.</p>
            <br>
            {{-- <div class="page-break"></div> --}}
            <div class="qr-code">
                <img src="data:image/svg;base64, {!! base64_encode(QrCode::format('svg')->size(300)->generate('Make me into an QrCode!')) !!} ">
            </div>
        </main>

        <footer>
            <h1>Varela + Cerca</h1>
        </footer>
    </body>
</html>
