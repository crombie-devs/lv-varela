@extends('adminlte::page')

@section('title', 'Beneficios')

@section('content_header')
    <h1>Beneficios</h1>
@stop

@section('content')

    @if(Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
    @endif

    <div id="alert" class="alert alert-danger" style="display:none;"></div>

    <div class="card card-outline card-info">
        <div class="card-header">
            <h3 class="card-title">Listado</h3>
            <div class="card-tools" @if ($user->rol == "ROLE_COMERCIO") hidden @endif>
                <a href="{{ url('admin/beneficios/create') }}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Nueva Beneficio</a>
            </div>
        </div>
        <div class="card-body">
            <table id="beneficios" class="table table-condensed table-hover">
                <thead>
                    <tr>
                        <th style="width: 20px">Id</th>
                        <th>Nombre</th>
                        <th>Activa</th>
                        <th>Destacada</th>
                        <th>Fecha Desde</th>
                        <th>Fecha Hasta</th>
                        <th>Canjeable</th>
                        <th>Canjeados</th>
                        <th style="width: 40px">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($entities as $entity)
                        <tr>
                            <td>{{ $entity->id }}</td>
                            <td>{{ $entity->nombre }}</td>
                            <td>
                                @if($entity->activa == 1)
                                    <h5><span class="badge badge-success">SI</span></h5>
                                @else
                                    <h5><span class="badge badge-danger">NO</span></h5>
                                @endif
                            </td>
                            <td>
                                @if($entity->destacada == 1)
                                    <h5><span class="badge badge-success">SI</span></h5>
                                @else
                                    <h5><span class="badge badge-danger">NO</span></h5>
                                @endif
                            </td>
                            <td>{{ \Carbon\Carbon::parse($entity->fecha_desde)->locale('es')->isoFormat('LL') }}</td>
                            <td>{{ \Carbon\Carbon::parse($entity->fecha_hasta)->locale('es')->isoFormat('LL') }}</td>
                            <input type="hidden" id="fecha_hasta" name="fecha_hasta" value="{{ $entity->fecha_hasta }}">
                            <td>
                                @if($entity->canjeable)
                                    {{ $entity->canjeable->nombre }}
                                @else
                                    No tiene asignado un canjeable.
                                @endif
                            </td>
                            <td>0</td>
                            <td>
                                <div class="btn-group">
                                    <a type="button" href="{{ url('admin/beneficios/' . $entity->id . '/edit') }}" class="btn btn-info">
                                        <i class="fas fa-search"></i>
                                    </a>
                                    @if ($user->rol == "ROLE_ADMIN")
                                        <a type="button" data-method="POST" onclick="return confirm('¿Seguro quieres borrar?. Esta acción no se puede deshacer')" href="{{ route('beneficios.deleted', $entity->id) }}" class="btn btn-danger">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    @endif
                                    <a type="button" onclick="return confirm('¿Desea generar un QR?.')" href="{{ url('admin/beneficios/' . $entity->id . '/qr') }}" class="btn btn-warning">
                                        <i class="fas fa-qrcode"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            
        </div>
    </div>
@stop

@section('css')
    
@stop

@section('js')
    <script> 
        $(function () {
            $("#beneficios").DataTable({
                paging: true,
                language: {
                    url: "{{ asset('/vendor/datatables/locale/es-ar.json') }}",
                }
            });

            var endDate = new Date($('#fecha_hasta').val());
            var now = new Date();

            if (endDate < now){
                message = "Hay ofertas que han caducido y debe actualizarlas";
                document.getElementById("alert").innerHTML = message;
                $('#alert').show();
            }
        });
    </script>
@stop