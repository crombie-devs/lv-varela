@extends('adminlte::page')

@section('title', 'Canjeados')

@section('content_header')
    <h1>Canjeados</h1>
@stop

@section('content')

    @if(Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
    @endif

    <div id="alert" class="alert alert-danger" style="display:none;"></div>

    <div class="card card-outline card-info">
        <div class="card-header">
            <h3 class="card-title">Listado de Canjeados</h3>
        </div>
        <div class="card-body">
            <table id="beneficios" class="table table-condensed table-hover">
                <thead>
                    <tr>
                        <th>Beneficio / Oferta</th>
                        <th>Usuario</th>
                        <th>Fecha Generado</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($entities as $entity)
                        <tr>
                            <td><strong>Benef. </strong>{{ $entity->beneficio->nombre }}</td>
                            <td>{{ (isset($entity->usuario->email)) ? $entity->usuario->email : 'No se reconoce el usuario.' }}</td>
                            <td>{{ \Carbon\Carbon::parse($entity->fecha_generado)->locale('es')->isoFormat('LL') }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            
        </div>
    </div>
@stop