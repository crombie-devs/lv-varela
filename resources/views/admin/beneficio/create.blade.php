@extends('adminlte::page')

@section('title', 'Beneficios')

@section('content_header')
    <h1>Beneficios</h1>
@stop

@section('content')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Nuevo</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        {{-- <form class="form-horizontal" action="{{ url('admin/beneficios/') }}" method="POST" enctype="multipart/form-data"> --}}
        {!! Form::open(['route' => 'beneficios.store', 'files' => true]) !!} <!-- 'autocomplete' => 'off' -->
        {{-- @csrf --}}
            {{-- {!! Form::token() !!} --}}
            @include('admin.beneficio.form')
        {{-- </form> --}}
        {!! Form::close() !!}
    </div>           
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop