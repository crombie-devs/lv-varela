@extends('adminlte::page')

@section('title', 'Beneficios')

@section('content_header')
    <h1>Beneficios</h1>
@stop

@section('content')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Editar</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        {{-- <form class="form-horizontal" action="{{ url('admin/beneficios/' . $entity->id) }}" method="POST" enctype="multipart/form-data"> --}}
        {!! Form::model($entity, ['route' => ['beneficios.update', $entity->id], 'files' => true, 'method' => 'put']) !!}
            {{-- @csrf --}}
            {{-- {{ method_field('PATCH') }} --}}
            @include('admin.beneficio.form')
        {!! Form::close() !!}
        </form>
    </div>           
@stop

@section('css')
    
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop