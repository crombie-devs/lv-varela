@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="card-body">
    <div class="form-group">
        <label for="nombre">Nombre</label>
        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingrese Nombre" value="{{ (isset($entity->nombre)) ? $entity->nombre : '' }}">
    </div>
    <div class="form-group">
        <label for="descripcion">Descripción</label>
        <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Ingrese Descripción" value="{{ (isset($entity->descripcion)) ? $entity->descripcion : '' }}">
    </div>
</div>

<div class="card-footer">
    <button type="submit" class="btn btn-info pull-right">Guardar</button>
</div>