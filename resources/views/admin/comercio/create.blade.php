@extends('adminlte::page')

@section('title', 'Comercios')

@section('content_header')
    <h1>Comercios</h1>
@stop

@section('content')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Nuevo</h3>
        </div>
        {!! Form::open(['route' => 'comercios.store', 'files' => true]) !!}
            @include('admin.comercio.form')
        {!! Form::close() !!}
    </div>           
@stop

@section('css')
    
@stop

@section('js')
    <script> 
        $(function () {
            
        });
    </script>
@stop