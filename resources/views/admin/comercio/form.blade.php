@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="card-body">
    <div class="form-group">
        {!! Form::label('nombre', 'Nombre') !!}
        {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Nombre', 'required' => true]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('descripcion', 'Descripción') !!}
        {!! Form::text('descripcion', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Descripción', 'required' => true]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('email', 'Email') !!}
        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Email']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('telefono', 'Teléfono') !!}
        {!! Form::text('telefono', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Teléfono']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('web', 'Web') !!}
        {!! Form::text('web', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Web']) !!}
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-6">
                <div class="image-wrapper">
                    @isset ($entity->imagen) 
                        <img id="picture" src="{{ Storage::url($entity->imagen->url) }}" alt="">
                    @else
                        <img id="picture" src="https://media.sproutsocial.com/uploads/2017/08/Social-Media-Video-Specs-Feature-Image.png" alt="">
                    @endisset
                </div>
            </div>
            <div class="col-6">
                {!! Form::label('imagen', 'Subir Imagen') !!}
                {!! Form::file('imagen', ['class' => 'form-control-file', 'accept' => 'image/*']) !!}
                {!! Form::hidden('imagen_url', null) !!}
            </div>
        </div>
    </div>
</div>

<div class="card-footer">
    {!! Form::submit('Guardar', ['class' => 'btn btn-info pull-right']) !!}
</div>

@section('css')
    <style>
        .image-wrapper {
            position: relative;
            padding-bottom: 56.25%;
        }

        .image-wrapper img {
            position: absolute;
            object-fit: cover;
            width: 100%;
            height: 100%;
        }
    </style>
@stop

@section('js')
    <script> 
        $(function () {
            document.getElementById("imagen").addEventListener('change', cambiarImagen);

            function cambiarImagen(event) {
                var file = event.target.files[0];

                var reader = new FileReader();
                reader.onload = (event) => {
                    document.getElementById("picture").setAttribute('src', event.target.result);
                };

                reader.readAsDataURL(file);
            }
        });
    </script>
@stop