@extends('adminlte::page')

@section('title', 'Comercios')

@section('content_header')
    <h1>Comercios</h1>
@stop

@section('content')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Editar</h3>
        </div>
        {!! Form::model($entity, ['route' => ['comercios.update', $entity->id], 'files' => true, 'method' => 'put']) !!}
            @include('admin.comercio.form')
        {!! Form::close() !!}
    </div>           
@stop

@section('css')
    
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop