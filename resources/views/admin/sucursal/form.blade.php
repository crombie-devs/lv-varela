@section('css')
    <link href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" rel="stylesheet"/>
@stop

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="card-body">
    <div class="form-group">
        <label for="nombre">Nombre</label>
        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingrese Nombre" value="{{ (isset($entity->nombre)) ? $entity->nombre : '' }}" @if ($user->rol == "ROLE_ADMIN") required @else readonly @endif>
    </div>
    <div class="form-group">
        <label for="telefono">Teléfono</label>
        <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Ingrese Teléfono" value="{{ (isset($entity->telefono)) ? $entity->telefono : '' }}" @if ($user->rol == "ROLE_ADMIN") required @else readonly @endif>
    </div>
    <div class="form-group">
        <label for="comercio_id">Selecciona Comercio</label>
        <select class="form-control" id="comercio_id" name="comercio_id" placeholder="Seleccione Comercio" @if ($user->rol == "ROLE_ADMIN") required @else disabled @endif>
            @foreach ($comercios as $key => $comercio)    
                <option @if (isset($entity) && $comercio->id == $entity->comercio_id) selected @endif value="{{ $comercio->id }}">{{ $comercio->nombre }}</option>
            @endforeach
        </select>
    </div>

    <div class="row" @if ($user->rol == "ROLE_COMERCIO") hidden @endif>
        <div class="col-2">
            <div class="form-group">
                <label for="ba">Búsqueda automática</label><br>
                <input class="ml-4" type="radio" name="optionsRadios" value="1" checked=""> 
            </div>
        </div>
        <div class="col-3">
            <div class="form-group">
                <label for="ba">Dirección manual (debera agregar market en el mapa)</label><br>
                <input class="ml-4" type="radio" name="optionsRadios" value="2"> 
            </div>
        </div>
    </div>
    
    <b style="color: black" @if ($user->rol == "ROLE_COMERCIO") hidden @endif>Buscar dirección</b>

    <select class="form-control" id="select_address_open" @if ($user->rol == "ROLE_COMERCIO") disabled @endif></select>
    
    <div id="osm-map" class="mt-3"></div>

    <div class="row mt-3">
        <div class="col-6">
            <div class="form-group">
                <label for="direccion">Dirección</label>
                <input readonly="readonly" id="input_address" type="text" class="form-control" name="direccion" placeholder="Direccion" value="{{ (isset($entity->direccion)) ? $entity->direccion : '' }}" @if ($user->rol == "ROLE_ADMIN") required @else disabled @endif>
            </div>
        </div>
        <div class="col-3">
            <label for="lat">Latitud</label>
            <input readonly="readonly" id="input_lat" type="text" class="form-control" placeholder="Latitud" name="lat" value="{{ (isset($entity->lat)) ? $entity->lat : '' }}" @if ($user->rol == "ROLE_ADMIN") required @else disabled @endif>
        </div>
        <div class="col-3">
            <label for="long">Longitud</label>
            <input readonly="readonly" id="input_lon" type="text" class="form-control" placeholder="Longitud" name="long" value="{{ (isset($entity->long)) ? $entity->long : '' }}" @if ($user->rol == "ROLE_ADMIN") required @else disabled @endif>
        </div>
    </div>
</div>

<div class="card-footer" @if ($user->rol == "ROLE_COMERCIO") hidden @endif>
    <button type="submit" class="btn btn-info pull-right">Guardar</button>
</div>

@section('js')
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
    <script> 
        var map = null;
        var popup = null;
        var macarte = null;
        var get_array = [];
        var current_market = null;
        
        function onMapClick(e) {
            popup.setLatLng(e.latlng);
            if (current_market != null) current_market.remove();
            var marker = L.marker(e.latlng).addTo(macarte);
            $('#input_lat').val(e.latlng.lat);
            $('#input_lon').val(e.latlng.lng);
            current_market = marker;
        }

        $( function() {
            popup = L.popup();

            var element = document.getElementById('osm-map');
            element.style = 'height:300px;';
            map = L.map(element);
            L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);
            var target = L.latLng('-34.652092759236204', '-58.44015240669251');
            macarte = map.setView(target, 12);

            @if (isset($entity))
                var target = L.latLng('<?php echo $entity->lat;?>', '<?php echo $entity->long;?>');
                macarte =   map.setView(target, 12);
                L.marker(target).addTo(map);
                map.on('click', onMapClick);
            @else
                var target = L.latLng('-34.652092759236204', '-58.44015240669251')
            @endif

            map.on('click', onMapClick);

            $('#select_address_open').select2({
                placeholder:"Direccion, Ciudad, Provincia",
                ajax: {
                    delay: 250,
                    url: 'https://nominatim.openstreetmap.org/search',
                    data: function(params) {
                        return {
                            q: params.term,
                            format: 'json',
                            polygon:1,
                            addressdetails:1,
                        };
                    },
                    processResults: function(data) {
                        get_array = data;
                        return {
                            results: $.map(data, function(item,i) {
                                return {
                                    text: item.display_name,
                                    id: item.place_id,
                                }
                            })
                        };
                    }
                }
            });

            $('#select_address_open').on('change', function() {
                let self = $(this).val();
                get_array.forEach(function (i,x) {
                    if (get_array[x].place_id == self) {
                        let target = L.latLng(get_array[x].lat, get_array[x].lon);
                        map.setView(target, 14);
                        if (current_market != null) current_market.remove();
                        current_market = L.marker(target).addTo(map);


                        $('#input_lat').val(get_array[x].lat);
                        $('#input_lon').val(get_array[x].lon);

                        $('#input_address').val(get_array[x].display_name);
                    }
                })
            });

            $('input[type=radio][name=optionsRadios]').change( function() {
                if (current_market != null) current_market.remove();
                if (this.value == 2) {
                    // $('#select_address_open').select2().next().hide();
                    $('#input_address').attr("readonly", false);
                    $('#input_lat').attr('readonly', false);
                    $('#input_lon').attr('readonly', false);
                } else {
                    // $('#select_address_open').select2().next().show();
                    $('#input_address').attr("readonly", true);
                    $('#input_lat').attr('readonly', true);
                    $('#input_lon').attr('readonly', true);
                }
            });
        });

    </script>
@stop