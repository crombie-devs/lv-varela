@extends('adminlte::page')

@section('title', 'Sucursales')

@section('content_header')
    <h1>Sucursales</h1>
@stop

@section('content')

    @if(Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
    @endif

    <div class="card card-outline card-info">
        <div class="card-header">
            <h3 class="card-title">Listado</h3>
            <div class="card-tools" @if ($user->rol == "ROLE_COMERCIO") hidden @endif>
                <a href="{{ url('admin/sucursales/create') }}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Nueva Sucursal</a>
            </div>
        </div>
        <div class="card-body">
            <table id="sucursales" class="table table-condensed table-hover">
                <thead>
                    <tr>
                        <th style="width: 20px">Id</th>
                        <th>Nombre</th>
                        <th>Comercio</th>
                        <th style="width: 40px">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($entities as $entity)
                        <tr>
                            <td>{{ $entity->id }}</td>
                            <td>{{ $entity->nombre }}</td>
                            @if ($entity->comercio)
                                <td>{{ $entity->comercio->nombre }}</td>    
                            @else
                                <td>No Tiene</td>
                            @endif
                            <td>
                                <div class="btn-group">
                                    <a type="button" href="{{ url('admin/sucursales/' . $entity->id . '/edit') }}" class="btn btn-info">
                                        <i class="fas fa-search"></i>
                                    </a>
                                    @if ($user->rol == "ROLE_ADMIN")
                                        <a type="button" data-method="POST" onclick="return confirm('¿Seguro quieres borrar?. Esta acción no se puede deshacer')" href="{{ route('sucursales.deleted', $entity->id) }}" class="btn btn-danger">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            
        </div>
    </div>
@stop

@section('css')
    
@stop

@section('js')
    <script> 
        $(function () {
            $("#sucursales").DataTable({
                paging: true,
                language: {
                    url: "{{ asset('/vendor/datatables/locale/es-ar.json') }}",
                }
            });
        });
    </script>
@stop