@extends('adminlte::page')

@section('title', 'Ofertas')

@section('content_header')
    <h1>Ofertas</h1>
@stop

@section('content')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Ver / Editar</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        {!! Form::model($entity, ['route' => ['ofertas.update', $entity->id], 'files' => true, 'method' => 'put']) !!}
            @include('admin.oferta.form')
        {!! Form::close() !!}
    </div>           
@stop

@section('css')
    
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop