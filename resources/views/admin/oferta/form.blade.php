@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="card-body">
    <div class="form-group">
        <div class="row">
            <div class="col-3">
                <div class="form-group">
                    {!! Form::label('activa', 'Activa') !!}<br>
                    @if ($user->rol == "ROLE_ADMIN") 
                        {!! Form::checkbox('activa', null, (isset($entity) && $entity->activa == 1) ? 1 : 0, ['class' => 'form-control']) !!}
                    @else 
                        {!! Form::checkbox('activa', null, (isset($entity) && $entity->activa == 1) ? 1 : 0, ['class' => 'form-control', 'readonly' => true]) !!}
                    @endif   
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    {!! Form::label('destacada', 'Destacada') !!}<br>
                    @if ($user->rol == "ROLE_ADMIN") 
                        {!! Form::checkbox('destacada', null, (isset($entity) && $entity->destacada == 1) ? 1 : 0, ['class' => 'form-control']) !!}
                    @else 
                        {!! Form::checkbox('destacada', null, (isset($entity) && $entity->destacada == 1) ? 1 : 0, ['class' => 'form-control', 'readonly' => true]) !!}
                    @endif    
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('nombre', 'Nombre') !!}
        @if ($user->rol == "ROLE_ADMIN") 
            {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Nombre', 'required' => true]) !!}
        @else 
            {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Nombre', 'readonly' => true]) !!}
        @endif
    </div>
    <div class="form-group">
        {!! Form::label('descripcion', 'Descripción') !!}
        @if ($user->rol == "ROLE_ADMIN") 
            {!! Form::text('descripcion', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Descripción', 'required' => true]) !!}
        @else 
            {!! Form::text('descripcion', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Descripción', 'readonly' => true]) !!}
        @endif
    </div>
    <div class="form-group">
        {!! Form::label('descripcion_corta', 'Descripción Corta') !!}
        @if ($user->rol == "ROLE_ADMIN") 
            {!! Form::text('descripcion_corta', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Descripción Corta', 'required' => true]) !!}
        @else 
            {!! Form::text('descripcion_corta', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Descripción Corta', 'readonly' => true]) !!}
        @endif
    </div>
    <div class="form-group">
        {!! Form::label('ganancia', 'Ganancia') !!}
        @if ($user->rol == "ROLE_ADMIN") 
            {!! Form::text('ganancia', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Ganancia', 'required' => true]) !!}
        @else 
            {!! Form::text('ganancia', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Ganancia', 'readonly' => true]) !!}
        @endif
    </div>
    <div class="form-group">
        {!! Form::label('categoria_id', 'Selecciona Categoria') !!}<br>
        @if ($user->rol == "ROLE_ADMIN") 
            {!! Form::select('categoria_id', $categorias, null, ['class' => 'form-control', 'required' => true]) !!}
        @else 
            {!! Form::select('categoria_id', $categorias, null, ['class' => 'form-control', 'disabled' => true]) !!}
        @endif
    </div>
    <div class="form-group">
        {!! Form::label('sucursales', 'Selecciona Sucursal/es') !!}<br>
        @if ($user->rol == "ROLE_ADMIN") 
            {!! Form::select('sucursales[]', $sucursales, isset($entity) ? $entity->sucursales : null, ['id' => 'sucursales', 'class' => 'form-control', 'multiple' => 'multiple', 'required' => true]) !!}
        @else 
            {!! Form::select('sucursales[]', $sucursales, isset($entity) ? $entity->sucursales : null, ['id' => 'sucursales', 'class' => 'form-control', 'multiple' => 'multiple', 'disabled' => true]) !!}
        @endif
    </div>
    {{-- <div class="form-group">
        <label for="sucursales">Selecciona Sucursal/es</label>
        <select class="form-control" id="sucursales" name="sucursales[]" placeholder="Seleccione Sucursal/es" multiple required>
            @foreach ($sucursales as $key => $sucursal)    
                @if (isset($entity))
                    @foreach ($entity->sucursales as $eSucursal)  
                        <option @if ($sucursal->id == $eSucursal->id) selected @endif value="{{ $sucursal->id }}">{{ $sucursal->nombre }}</option>
                    @endforeach
                @else
                    <option value="{{ $sucursal->id }}">{{ $sucursal->nombre }}</option>
                @endif
            @endforeach
        </select>
    </div> --}}
    <div class="form-group">
        <div class="row">
            <div class="col-6">
                {!! Form::label('fecha_desde', 'Fecha Desde:') !!}<br>
                @if ($user->rol == "ROLE_ADMIN") 
                    {!! Form::date('fecha_desde', (isset($entity->fecha_desde)) ? $entity->fecha_desde : '', ['required' => true]) !!}
                @else 
                    {!! Form::date('fecha_desde', (isset($entity->fecha_desde)) ? $entity->fecha_desde : '', ['disabled' => true]) !!}
                @endif
            </div>
            <div class="col-6">
                {!! Form::label('fecha_hasta', 'Fecha Hasta:') !!}<br>
                @if ($user->rol == "ROLE_ADMIN") 
                    {!! Form::date('fecha_hasta', (isset($entity->fecha_hasta)) ? $entity->fecha_hasta : '', ['required' => true]) !!}
                @else 
                    {!! Form::date('fecha_hasta', (isset($entity->fecha_hasta)) ? $entity->fecha_hasta : '', ['disabled' => true]) !!}
                @endif
            </div>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('dias', 'Selecciona Día/s') !!}<br>
        @if ($user->rol == "ROLE_ADMIN") 
            {!! Form::select('dias[]', $dias, isset($entity) ? $entity->dias : null, ['id' => 'dias', 'class' => 'form-control', 'multiple' => 'multiple', 'required' => true]) !!}
        @else 
            {!! Form::select('dias[]', $dias, isset($entity) ? $entity->dias : null, ['id' => 'dias', 'class' => 'form-control', 'multiple' => 'multiple', 'disabled' => true]) !!}
        @endif
    </div>
    <div class="form-group">
        <div class="row">
            @if ($user->rol == "ROLE_ADMIN") 
                <div class="col-6">
                    <div class="image-wrapper">
                        @isset ($entity->imagen_cuadro) 
                            <img id="pictureCuadro" src="{{ Storage::url($entity->imagen_cuadro->url) }}" alt="">
                        @else
                            <img id="pictureCuadro" src="https://media.sproutsocial.com/uploads/2017/08/Social-Media-Video-Specs-Feature-Image.png" alt="">
                        @endisset
                    </div>
                </div>
                <div class="col-6">
                    {!! Form::label('imagen_cuadro', 'Subir Imagen Cuadro') !!}
                    {!! Form::file('imagen_cuadro', ['class' => 'form-control-file', 'accept' => 'image/*']) !!}
                    {!! Form::hidden('imagen_cuadro_url', null) !!}
                </div>
            @else 
                <div class="col-6">
                    <div class="image-wrapper">
                        @isset ($entity->imagen_cuadro) 
                            <img id="pictureCuadro" src="{{ Storage::url($entity->imagen_cuadro->url) }}" alt="">
                        @else
                            <img id="pictureCuadro" src="https://media.sproutsocial.com/uploads/2017/08/Social-Media-Video-Specs-Feature-Image.png" alt="">
                        @endisset
                    </div>
                </div>
                <div class="col-6">
                    <div class="image-wrapper">
                        @isset ($entity->imagen_cabecera) 
                            <img id="pictureCabecera" src="{{ Storage::url($entity->imagen_cabecera->url) }}" alt="">
                        @else
                            <img id="pictureCabecera" src="https://media.sproutsocial.com/uploads/2017/08/Social-Media-Video-Specs-Feature-Image.png" alt="">
                        @endisset
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            @if ($user->rol == "ROLE_ADMIN") 
                <div class="col-6">
                    <div class="image-wrapper">
                        @isset ($entity->imagen_cabecera) 
                            <img id="pictureCabecera" src="{{ Storage::url($entity->imagen_cabecera->url) }}" alt="">
                        @else
                            <img id="pictureCabecera" src="https://media.sproutsocial.com/uploads/2017/08/Social-Media-Video-Specs-Feature-Image.png" alt="">
                        @endisset
                    </div>
                </div>
                <div class="col-6">
                    {!! Form::label('imagen_cabecera', 'Subir Imagen Cabecera') !!}
                    {!! Form::file('imagen_cabecera', ['class' => 'form-control-file', 'accept' => 'image/*']) !!}
                    {!! Form::hidden('imagen_cabecera_url', null) !!}
                </div>
            @endif
        </div>
    </div>
</div>

<div class="card-footer" @if ($user->rol == "ROLE_COMERCIO") hidden @endif>
    {!! Form::submit('Guardar', ['class' => 'btn btn-info pull-right']) !!}
</div>

@section('css')
    <style>
        .image-wrapper {
            position: relative;
            padding-bottom: 56.25%;
        }

        .image-wrapper img {
            position: absolute;
            object-fit: cover;
            width: 100%;
            height: 100%;
        }
    </style>
@stop

@section('js')
    <script> 
        $(function () {
            $.fn.bootstrapSwitch.defaults.onText = 'Si';
            $.fn.bootstrapSwitch.defaults.offText = 'No';
            $.fn.bootstrapSwitch.defaults.labelWidth = 0;
            $.fn.bootstrapSwitch.defaults.onColor = 'success';
            $.fn.bootstrapSwitch.defaults.offColor = 'danger';
            $.fn.bootstrapSwitch.defaults.inverse = true;

            $("[name='activa']").bootstrapSwitch();
            $("[name='destacada']").bootstrapSwitch();
            
            $("[name='sucursales[]']").select2();
            $("[name='dias[]']").select2();

            document.getElementById("imagen_cuadro").addEventListener('change', cambiarImagenCuadro);

            function cambiarImagenCuadro(event) {
                var file = event.target.files[0];

                var reader = new FileReader();
                reader.onload = (event) => {
                    document.getElementById("pictureCuadro").setAttribute('src', event.target.result);
                };

                reader.readAsDataURL(file);
            }

            document.getElementById("imagen_cabecera").addEventListener('change', cambiarImagenCabecera);

            function cambiarImagenCabecera(event) {
                var file = event.target.files[0];

                var reader = new FileReader();
                reader.onload = (event) => {
                    document.getElementById("pictureCabecera").setAttribute('src', event.target.result);
                };

                reader.readAsDataURL(file);
            }
        });
    </script>
@stop