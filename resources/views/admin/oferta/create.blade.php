@extends('adminlte::page')

@section('title', 'Ofertas')

@section('content_header')
    <h1>Ofertas</h1>
@stop

@section('content')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Nuevo</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        {!! Form::open(['route' => 'ofertas.store', 'files' => true]) !!} <!-- 'autocomplete' => 'off' -->
            @include('admin.oferta.form')
        {!! Form::close() !!}
    </div>           
@stop

@section('css')
    
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop