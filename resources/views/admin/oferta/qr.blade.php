@extends('adminlte::page')

@section('title', 'Ofertas')

@section('content_header')
    <h1>Ofertas</h1>
@stop

@section('content')

    @if(Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
    @endif

    <div class="card card-outline card-info">
        <div class="card-header">
            <h3 class="card-title">Código QR de Oferta</h3>
        </div>
        <div class="card-body">
            <p><strong>Título: </strong>{{ $entity->nombre }}</p>
            <p><strong>Descripción: </strong>{{ $entity->descripcion }}</p>
            <p><strong>Fecha Desde: </strong>{{ \Carbon\Carbon::parse($entity->fecha_desde)->locale('es')->isoFormat('LL') }}</p>
            <p><strong>Fecha Hasta: </strong>{{ \Carbon\Carbon::parse($entity->fecha_hasta)->locale('es')->isoFormat('LL') }}</p>
            <p><strong>Sucursales: </strong><br>
                @if (isset($entity->sucursales) && count($entity->sucursales) <> 0)
                    @foreach($entity->sucursales as $sucursal)
                        &nbsp;&nbsp;&nbsp;&nbsp;- {{ $sucursal->nombre }}<br>
                    @endforeach
                @else
                    No tiene.
                @endif
            </p>
            <p><strong>Días: </strong><br>
                @if (isset($entity->dias) && count($entity->dias) <> 0)
                    @foreach($entity->dias as $dia)
                        &nbsp;&nbsp;&nbsp;&nbsp;- {{ $dia->nombre }}<br>
                    @endforeach
                @else
                    No tiene.
                @endif
            </p>
            {!! QrCode::size(300)->generate('oferta-' . $entity->id) !!}
        </div>
        <div class="card-footer">
            <a href="{{ route('ofertas.printPDF', $entity->id) }}" class="btn btn-danger pull-right"><i class="fa fa-file-pdf"></i> Generar PDF</a>
        </div>
    </div>
@stop

@section('js')
    <script> 
        $(function () {
            
        });
    </script>
@stop