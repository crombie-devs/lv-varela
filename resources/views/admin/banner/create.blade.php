@extends('adminlte::page')

@section('title', 'Banners')

@section('content_header')
    <h1>Banners</h1>
@stop

@section('content')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Nuevo</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form class="form-horizontal" action="{{ url('admin/banners/') }}" method="POST" enctype="multipart/form-data">
        @csrf
            @include('admin.banner.form')
        </form>
    </div>           
@stop

@section('css')
    
@stop

@section('js')

@stop