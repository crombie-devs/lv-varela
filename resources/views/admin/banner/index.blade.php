@extends('adminlte::page')

@section('title', 'Banners')

@section('content_header')
    <h1>Banners</h1>
@stop

@section('content')

    @if(Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
    @endif

    <div class="card card-outline card-info">
        <div class="card-header">
            <h3 class="card-title">Listado</h3>
            <div class="card-tools">
                <a href="{{ url('admin/banners/create') }}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Nueva Banner</a>
            </div>
        </div>
        <div class="card-body">
            <table id="banners" class="table table-condensed table-hover">
                <thead>
                    <tr>
                        <th style="width: 20px">Id</th>
                        <th>Oferta / Beneficio</th>
                        <th>Activo</th>
                        <th>Destacado</th>
                        <th style="width: 40px">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($entities as $entity)
                        <tr>
                            <td>{{ $entity->id }}</td>
                            @if ($entity->oferta_id)
                                <td><strong>Of.</strong>: {{ $entity->oferta->nombre }}</td>    
                            @elseif ($entity->beneficio_id)
                                <td><strong>Be.</strong>: {{ $entity->beneficio->nombre }}</td>
                            @else
                                <td>No Tiene</td>
                            @endif
                            <td>
                                @if($entity->activo == 1)
                                    <h5><span class="badge badge-success">SI</span></h5>
                                @else
                                    <h5><span class="badge badge-danger">NO</span></h5>
                                @endif
                            </td>
                            <td>
                                @if($entity->destacado == 1)
                                    <h5><span class="badge badge-success">SI</span></h5>
                                @else
                                    <h5><span class="badge badge-danger">NO</span></h5>
                                @endif
                            </td>
                            <td>
                                <div class="btn-group">
                                    <a type="button" href="{{ url('admin/banners/' . $entity->id . '/edit') }}" class="btn btn-info">
                                      <i class="fas fa-search"></i>
                                    </a>
                                    <a type="button" data-method="POST" onclick="return confirm('¿Seguro quieres borrar?. Esta acción no se puede deshacer')" href="{{ route('banners.deleted', $entity->id) }}" class="btn btn-danger">
                                      <i class="fas fa-trash"></i>
                                    </a>
                                    @if($entity->beneficio_id && $entity->destacado != 1)
                                        <a type="button" data-method="DELETE" onclick="return confirm('¿Quieres marcar el banner como destacado? Solo se puede tener un banner destacado!.')" href="{{ route('banners.distinguish', $entity->id) }}" class="btn btn-warning">
                                            <i class="fas fa-star"></i>
                                        </a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            
        </div>
    </div>
@stop

@section('css')
    
@stop

@section('js')
    <script> 
        $(function () {
            $("#banners").DataTable({
                paging: true,
                language: {
                    url: "{{ asset('/vendor/datatables/locale/es-ar.json') }}",
                }
            });
        });
    </script>
@stop