@extends('adminlte::page')

@section('title', 'Banners')

@section('content_header')
    <h1>Banners</h1>
@stop

@section('content')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Editar</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form class="form-horizontal" action="{{ url('admin/banners/' . $entity->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            {{ method_field('PATCH') }}
            @include('admin.banner.form')
        </form>
    </div>           
@stop

@section('css')
    
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop