@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="card-body">
    <div class="form-group">
        <div class="row">
            <div class="col-3">
                <div class="form-group">
                    <label for="activo">Activo</label><br>
                    <input type="checkbox" id="activo" name="activo" data-on-text="Si" data-off-text="No" data-label-width="0" data-on-color="success" data-off-color="danger" data-inverse="true" @if (isset($entity->activo) && $entity->activo == 1) checked @endif>
                </div>
            </div>
            <div class="col-3" style="display: none">
                <input type="checkbox" id="destacado" name="destacado" @if (isset($entity->destacado) && $entity->destacado == 1) checked @endif>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-4">
                <label for="tipo">Seleccione Tipo</label><br>
                <input type="checkbox" id="tipo" name="tipo" data-on-text="Oferta" data-off-text="Beneficio" data-label-width="0" data-inverse="true" data-size="large">
            </div>
            <div class="col-8" id="oferta_form" style="display:none;">
                <label for="oferta_id">Selecciona Oferta</label>
                <select class="form-control" id="oferta_id" name="oferta_id" placeholder="Seleccione Oferta">
                    <option value=''>Sin Oferta</option>
                    @foreach ($ofertas as $key => $oferta)    
                        <option @if (isset($entity) && $oferta->id == $entity->oferta_id) selected @endif value="{{ $oferta->id }}">{{ $oferta->nombre }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-8" id="beneficio_form">
                <label for="beneficio_id">Selecciona Beneficio</label>
                <select class="form-control" id="beneficio_id" name="beneficio_id" placeholder="Seleccione Beneficio">
                    <option value=''>Sin Beneficio</option>
                    @foreach ($beneficios as $key => $beneficio)    
                        <option @if (isset($entity) && $beneficio->id == $entity->beneficio_id) selected @endif value="{{ $beneficio->id }}">{{ $beneficio->nombre }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>

<div class="card-footer">
    <button type="submit" class="btn btn-info pull-right">Guardar</button>
</div>

@section('js')
    <script> 
        $(function () {
            $("[name='activo']").bootstrapSwitch();
            $("[name='destacado']").bootstrapSwitch();
            $("[name='tipo']").bootstrapSwitch();
            $('#oferta_id').prop('required', true);
            $('#beneficio_id').prop('required', true);
            $('#tipo').on('switchChange.bootstrapSwitch', function (event, state) {
                if (state) {
                    $('#oferta_form').show();
                    $('#beneficio_form').hide();
                    $('#beneficio_id').val('');

                    $('#oferta_id').prop('required', true);
                    $('#beneficio_id').prop('required', false);
                } else {
                    $('#oferta_form').hide();
                    $('#oferta_id').val('');
                    $('#beneficio_form').show();

                    $('#oferta_id').prop('required', false);
                    $('#beneficio_id').prop('required', true);
                }
            });
        });
    </script>
@stop