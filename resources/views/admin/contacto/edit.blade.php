@extends('adminlte::page')

@section('title', 'Contactos')

@section('content_header')
    <h1>Contactos</h1>
@stop

@section('content')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Editar</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        {!! Form::model($entity, ['route' => ['contactos.update', $entity->id], 'method' => 'put']) !!}
            @include('admin.canjeable.form')
        {!! Form::close() !!}
    </div>           
@stop

@section('css')
    
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop