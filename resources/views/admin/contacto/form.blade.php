@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="card-body">
    <div class="form-group">
        {!! Form::label('email', 'Email') !!}
        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Email', 'required' => true]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('motivo', 'Motivo') !!}
        {!! Form::text('motivo', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Motivo', 'required' => true]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('mensaje', 'Mensaje') !!}
        {!! Form::text('mensaje', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Mensaje', 'required' => true]) !!}
    </div>
</div>

<div class="card-footer">
    <button type="submit" class="btn btn-info pull-right">Guardar</button>
</div>

@section('js')
    <script> 
        $(function () {
            
        });
    </script>
@stop