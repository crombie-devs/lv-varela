@extends('adminlte::page')

@section('title', 'Plataforma de Gestión Online')

@section('content_header')
    <h1>Plataforma de Gestión Online</h1>
@stop

@section('content')
    <p>Bienvenido al Panel Admin.</p>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop