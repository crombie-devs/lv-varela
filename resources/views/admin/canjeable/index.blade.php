@extends('adminlte::page')

@section('title', 'Canjeables')

@section('content_header')
    <h1>Canjeables</h1>
@stop

@section('content')

    @if(Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
    @endif

    <div class="card card-outline card-info">
        <div class="card-header">
            <h3 class="card-title">Listado</h3>
            <div class="card-tools">
                <a href="{{ url('admin/canjeables/create') }}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Nuevo Canjeable</a>
            </div>
        </div>
        <div class="card-body">
            <table id="canjeables" class="table table-condensed table-hover">
                <thead>
                    <tr>
                        <th style="width: 20px">Id</th>
                        <th>Nombre</th>
                        <th>Sucursal</th>
                        <th style="width: 40px">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($entities as $entity)
                        <tr>
                            <td>{{ $entity->id }}</td>
                            <td>{{ $entity->nombre }}</td>
                            <td>
                                @if (isset($entity->sucursales) && count($entity->sucursales) <> 0)
                                    @foreach($entity->sucursales as $sucursal)
                                        - {{ $sucursal->nombre }}<br>
                                    @endforeach
                                @else
                                    No tiene.
                                @endif
                            </td>
                            <td>
                                <div class="btn-group">
                                    <a type="button" href="{{ url('admin/canjeables/' . $entity->id . '/edit') }}" class="btn btn-info">
                                      <i class="fas fa-search"></i>
                                    </a>
                                    <a type="button" data-method="POST" onclick="return confirm('¿Seguro quieres borrar?. Esta acción no se puede deshacer')" href="{{ route('canjeables.deleted', $entity->id) }}" class="btn btn-danger">
                                        <i class="fas fa-trash"></i>
                                      </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            
        </div>
    </div>
@stop

@section('js')
    <script> 
        $(function () {
            $("#canjeables").DataTable({
                paging: true,
                language: {
                    url: "{{ asset('/vendor/datatables/locale/es-ar.json') }}",
                }
            });
        });
    </script>
@stop