@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="card-body">
    <div class="form-group">
        {!! Form::label('nombre', 'Nombre') !!}
        {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Nombre', 'required' => true]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('sucursales', 'Selecciona Sucursal/es') !!}<br>
        {!! Form::select('sucursales[]', $sucursales, isset($entity) ? $entity->sucursales : null, ['id' => 'sucursales', 'class' => 'form-control', 'multiple' => 'multiple', 'required' => true]) !!}
    </div>
</div>

<div class="card-footer">
    <button type="submit" class="btn btn-info pull-right">Guardar</button>
</div>

@section('js')
    <script> 
        $(function () {
            $("[name='sucursales[]']").select2();
        });
    </script>
@stop