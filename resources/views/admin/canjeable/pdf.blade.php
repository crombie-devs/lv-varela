@extends('adminlte::page')

@section('title', 'Canjeables')

@section('content_header')
    <h1>Canjeables</h1>
@stop

@section('content')

    <div class="card card-outline card-info">
        <div class="card-header">
            <h3 class="card-title">Código QR Canjeable</h3>
        </div>
        <div class="card-body">
            {!! QrCode::size(300)->generate('RemoteStack') !!}
        </div>
        <div class="card-footer">

        </div>
    </div>
@stop

@section('js')
    <script> 
        $(function () {
            
        });
    </script>
@stop