@extends('adminlte::page')

@section('title', 'Usuarios Mobile')

@section('content_header')
    <h1>Usuarios Mobile</h1>
@stop

@section('content')

    @if(Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
    @endif

    <div class="card card-outline card-info">
        <div class="card-header">
            <h3 class="card-title">Listado</h3>
            <div class="card-tools">
                {{-- <a href="{{ url('admin/usuarios/mobile/create') }}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Nueva Usuario Mobile</a> --}}
            </div>
        </div>
        <div class="card-body">
            <table id="usuarios-app" class="table table-condensed table-hover">
                <thead>
                    <tr>
                        <th style="width: 20px">Id</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Rol</th>
                        <th style="width: 40px">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($entities as $entity)
                        <tr>
                            <td>{{ $entity->id }}</td>
                            <td>{{ $entity->nombre }}</td>
                            <td>{{ $entity->apellido }}</td>
                            <td>{{ $entity->rol }}</td>
                            <td>
                                <div class="btn-group">
                                    <a type="button" href="{{ url('admin/usuarios/mobile/' . $entity->id . '/edit') }}" class="btn btn-info">
                                        <i class="fas fa-search"></i>
                                    </a>
                                    <a type="button" data-method="POST" onclick="return confirm('¿Seguro quieres borrar?. Esta acción no se puede deshacer')" href="{{ route('mobile.deleted', $entity->id) }}" class="btn btn-danger">
                                        <i class="fas fa-trash"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            
        </div>
    </div>
@stop

@section('css')
    
@stop

@section('js')
    <script> 
        $(function () {
            $("#usuarios-app").DataTable({
                paging: true,
                language: {
                    url: "{{ asset('/vendor/datatables/locale/es-ar.json') }}",
                }
            });
        });
    </script>
@stop