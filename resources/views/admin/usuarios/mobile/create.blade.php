@extends('adminlte::page')

@section('title', 'Usuarios App')

@section('content_header')
    <h1>Usuarios App</h1>
@stop

@section('content')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Nuevo</h3>
        </div>
        {!! Form::open(['route' => 'mobile.store']) !!}
            @include('admin.usuarios.mobile.form')
        {!! Form::close() !!}
    </div>           
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop