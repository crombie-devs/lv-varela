@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="card-body">
    <div class="form-group">
        {!! Form::label('rol', 'Selecciona Rol') !!}<br>
        {!! Form::select('rol', $roles, null, ['class' => 'form-control', 'required' => true]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('id', 'ID') !!}
        {!! Form::text('id', null, ['class' => 'form-control', 'placeholder' => 'Ingrese ID', 'readonly' => true]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('email', 'Email') !!}
        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Email', 'readonly' => true]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('nombre', 'Nombre') !!}
        {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Nombre', 'readonly' => true]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('apellido', 'Apellido') !!}
        {!! Form::text('apellido', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Apellido', 'readonly' => true]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('fecha_nacimiento', 'Fecha de Nacimiento:') !!}
        {!! Form::hidden('fecha_nacimiento', null) !!}
        <div class="input-group date" id="fecha_nac" data-target-input="nearest">
            <input type="text" name="fecha_nac" class="form-control datetimepicker-input" data-target="#fecha_nac" value="{{ (isset($entity->fecha_nacimiento)) ? $entity->fecha_nacimiento : '' }}" disabled>
            <div class="input-group-append" data-target="#fecha_nac" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
            </div>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('barrio', 'Barrio') !!}
        {!! Form::text('barrio', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Barrio', 'readonly' => true]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('device_key', 'Id Dispositivo') !!}
        {!! Form::text('device_key', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Id Dispositivo', 'readonly' => true]) !!}
    </div>
</div>

<div class="card-footer">
    {!! Form::submit('Guardar', ['class' => 'btn btn-info pull-right']) !!}
</div>

@section('js')
    <script> 
        $(function () {
            $('#fecha_nac').datetimepicker({
                locale: moment.locale('es'),
                format: 'LL',
            });
            $('#fecha_nac').on("change.datetimepicker", function (e) {
                let fecha_nac = moment(e.date).format('YYYY-MM-DD'); 
                $('input[name="fecha_nacimiento"]').val(fecha_nac);
            });
        });
    </script>
@stop