@extends('adminlte::page')

@section('title', 'Usuarios App')

@section('content_header')
    <h1>Usuarios App</h1>
@stop

@section('content')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Editar</h3>
        </div>
        {!! Form::model($entity, ['route' => ['mobile.update', $entity->id], 'method' => 'put']) !!}
            @include('admin.usuarios.mobile.form')
        {!! Form::close() !!}
        </form>
    </div>           
@stop

@section('css')
    
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop