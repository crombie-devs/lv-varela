@extends('adminlte::page')

@section('title', 'Usuarios Backend')

@section('content_header')
    <h1>Usuarios Backend</h1>
@stop

@section('content')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Nuevo</h3>
        </div>
        {!! Form::open(['route' => 'backend.store']) !!}
            @include('admin.usuarios.backend.form')
        {!! Form::close() !!}
    </div>           
@stop

@section('css')
    
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop