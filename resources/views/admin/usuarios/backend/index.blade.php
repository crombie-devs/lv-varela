@extends('adminlte::page')

@section('title', 'Usuarios Backend')

@section('content_header')
    <h1>Usuarios Backend</h1>
@stop

@section('content')

    @if(Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
    @endif

    <div class="card card-outline card-info">
        <div class="card-header">
            <h3 class="card-title">Listado</h3>
            <div class="card-tools">
                <a href="{{ url('admin/usuarios/backend/create') }}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Nueva Usuario Backend</a>
            </div>
        </div>
        <div class="card-body">
            <table id="usuarios-back" class="table table-condensed table-hover">
                <thead>
                    <tr>
                        <th style="width: 20px">Id</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Rol</th>
                        <th>Comercio</th>
                        {{-- <th>Activo</th> --}}
                        <th style="width: 40px">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($entities as $entity)
                        @if ($entity->id > 3)
                            <tr>
                                <td>{{ $entity->id }}</td>
                                <td>{{ $entity->name }}</td>
                                <td>{{ $entity->email }}</td>
                                <td>{{ $entity->rol }}</td>
                                @if ($entity->comercio)
                                    <td>{{ $entity->comercio->nombre }}</td>    
                                @else
                                    @if ($entity->rol == "ROLE_COMERCIO")
                                        <td>No tiene comercio asignado.</td>
                                    @else
                                        <td>No es un usuario comercio.</td>
                                    @endif
                                @endif
                                {{-- <td>{{ $entity->comercio->nombre }}</td> si es usuario back con rol comercio --}}
                                <td>
                                    <div class="btn-group">
                                        <a type="button" href="{{ url('admin/usuarios/backend/' . $entity->id . '/edit') }}" class="btn btn-info">
                                            <i class="fas fa-search"></i>
                                        </a>
                                        <a type="button" data-method="POST" onclick="return confirm('¿Seguro quieres borrar?. Esta acción no se puede deshacer')" href="{{ route('backend.deleted', $entity->id) }}" class="btn btn-danger">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            
        </div>
    </div>
@stop

@section('css')
    
@stop

@section('js')
    <script> 
        $(function () {
            $("#usuarios-back").DataTable({
                paging: true,
                language: {
                    url: "{{ asset('/vendor/datatables/locale/es-ar.json') }}",
                }
            });
        });
    </script>
@stop