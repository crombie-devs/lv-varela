@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="card-body">
    <div class="form-group">
        {!! Form::label('rol', 'Selecciona Rol') !!}<br>
        {!! Form::select('rol', $roles, null, ['class' => 'form-control', 'required' => true]) !!}
    </div>
    <div class="form-group" id="comercio" style="display: none;">
        {!! Form::label('comercio_id', 'Selecciona Comercio') !!}<br>
        {!! Form::select('comercio_id', $comercios, null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('name', 'Nombre') !!}
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Nombre', 'required' => true]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('email', 'Email') !!}
        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Email', 'required' => true]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('password', 'Password') !!}
        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Ingrese Password', 'required' => true]) !!}
    </div>
</div>

<div class="card-footer">
    {!! Form::submit('Guardar', ['class' => 'btn btn-info pull-right']) !!}
</div>

@section('js')
    <script> 
        $(function () {
            $('#rol').change(function(e) {
                var rol = $('#rol').val();
                console.log(rol);
                if (rol == "ROLE_ADMIN") {
                    $('#comercio').hide();
                    $('#comercio_id').val(null);
                } else {
                    $('#comercio').show();
                    $('#comercio_id').val(1);
                }
            });
        });
    </script>
@stop