@extends('adminlte::page')

@section('title', 'Usuarios Backend')

@section('content_header')
    <h1>Usuarios Backend</h1>
@stop

@section('content')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Editar</h3>
        </div>
        {!! Form::model($entity, ['route' => ['backend.update', $entity->id], 'method' => 'put']) !!}
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
            <div class="card-body">
                <div class="form-group">
                    {!! Form::label('rol', 'Selecciona Rol') !!}<br>
                    {!! Form::select('rol', $roles, null, ['class' => 'form-control', 'required' => true]) !!}
                </div>
                <div class="form-group" id="comercio" style="display: none;">
                    {!! Form::label('comercio_id', 'Selecciona Comercio') !!}<br>
                    {!! Form::select('comercio_id', $comercios, null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('name', 'Nombre') !!}
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Nombre', 'required' => true]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('email', 'Email') !!}
                    {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Email', 'required' => true]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('password', 'Password') !!}
                    {{-- {!! Form::text('password', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Password', 'required' => true]) !!} --}}
                    <br> - Para cambiar la contraseña debe hacerlo ingresando aquí.
                    <a href="{{ route('backend.changePassword', $entity->id) }}">Actualizar Contraseña</a>
                </div>
            </div>
            
            <div class="card-footer">
                {!! Form::submit('Guardar', ['class' => 'btn btn-info pull-right']) !!}
            </div>
            
            @section('js')
                <script> 
                    $(function () {
                        $('#rol').change(function(e) {
                            var rol = $('#rol').val();
                            console.log(rol);
                            if (rol == "ROLE_ADMIN") {
                                $('#comercio').hide();
                                $('#comercio_id').val(null);
                            } else {
                                $('#comercio').show();
                                $('#comercio_id').val(1);
                            }
                        });
                    });
                </script>
            @stop
        {!! Form::close() !!}
        </form>
    </div>           
@stop

@section('css')
    
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop