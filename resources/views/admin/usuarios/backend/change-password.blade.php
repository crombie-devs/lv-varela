@extends('adminlte::page')

@section('title', 'Usuarios Backend')

@section('content_header')
    <h1>Usuarios Backend</h1>
@stop

@section('content')

    @if(Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
    @endif


    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Actualizar Contraseña</h3>
        </div>
        <div class="card-body">
            <form action="{{ route('backend.updatePassword', $id) }}" id="change_password_form" method="post">
                @csrf
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group">
                    <label for="old_password">Contraseña Anterior</label>
                    <input type="password" name="old_password" class="form-control" id="old_password">
                    @if($errors->any('old_password'))
                        <span class="text-danger">{{ $errors->first('old_password') }}</span>
                    @endif
                </div> 
                <div class="form-group">
                    <label for="new_password">Contraseña Nueva</label>
                    <input type="password" name="new_password" class="form-control" id="new_password">
                    @if($errors->any('new_password'))
                        <span class="text-danger">{{ $errors->first('new_password') }}</span>
                    @endif
                </div> 
                <div class="form-group">
                    <label for="confirm_password">Repetir Contraseña Nueva</label>
                    <input type="password" name="confirm_password" class="form-control" id="confirm_password">
                    @if($errors->any('confirm_password'))
                        <span class="text-danger">{{ $errors->first('confirm_password') }}</span>
                    @endif
                </div> 
                <button type="submit" class="btn btn-primary">Actualizar Contraseña</button>
            </form>
        </div>
    </div>           
@stop

@section('css')
    
@stop

@section('js')
    <script> 
        $('#change_password_form').validate({
            ignore: '.ignore',
            errorClass: 'invalid',
            validClass: 'success',
            rules: {
                old_password: {
                    required: true,
                    minlength: 6,
                    maxlength: 100
                },
                new_password: {
                    required: true,
                    minlength: 6,
                    maxlength: 100
                },
                confirm_password: {
                    required: true,
                    equalTo: '#new_password'
                },
            },
            messages: {
                old_password: {
                    required: "Ingrese la contraseña actual.",
                },
                new_password: {
                    required: "Ingrese la nueva contraseña.",
                },
                confirm_password: {
                    required: "Necesita confirmar la nueva contraseña",
                },
            },
            submitHandler: function(form) {
                $.LoadingOverlay("show");
                form.submit();
            }
        });
    </script>
@stop