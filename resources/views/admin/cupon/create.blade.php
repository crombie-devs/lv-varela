@extends('adminlte::page')

@section('title', 'Cupones')

@section('content_header')
    <h1>Cupones</h1>
@stop

@section('content')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Nuevo</h3>
        </div>
        {!! Form::open(['route' => 'cupones.store']) !!}
            @include('admin.cupon.form')
        {!! Form::close() !!}
    </div>           
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop