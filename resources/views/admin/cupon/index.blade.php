@extends('adminlte::page')

@section('title', 'Cupones')

@section('content_header')
    <h1>Cupones</h1>
@stop

@section('content')

    @if(Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
    @endif

    <div class="card card-outline card-info">
        <div class="card-header">
            <h3 class="card-title">Listado</h3>
            <div class="card-tools">
                <a href="{{ url('admin/cupones/create') }}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Nuevo Cupon</a>
            </div>
        </div>
        <div class="card-body">
            <table id="cupones" class="table table-condensed table-hover">
                <thead>
                    <tr>
                        <th style="width: 20px">Id</th>
                        <th>Beneficio</th>
                        <th>Usuario</th>
                        <th>Canjeado</th>
                        <th>Fecha de Canje</th>
                        <th style="width: 40px">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($entities as $entity)
                        <tr>
                            <td>{{ $entity->id }}</td>
                            <td>
                                @if (isset($entity->beneficio))
                                    {{ $entity->beneficio->nombre }}
                                @endif
                            </td>
                            <td>
                                @if (isset($entity->usuario))
                                    {{ $entity->usuario->email }}
                                @endif
                            </td>
                            <td>
                                @if($entity->canjeado == 1)
                                    <h5><span class="badge badge-success">SI</span></h5>
                                @else
                                    <h5><span class="badge badge-danger">NO</span></h5>
                                @endif
                            </td>
                            <td>
                            @if($entity->fecha_canje)
                                {{ \Carbon\Carbon::parse($entity->fecha_canje)->locale('es')->isoFormat('LL') }}
                            @else
                                Todavia no fue canjeado.
                            @endif
                            </td>
                            <td>
                                <div class="btn-group">
                                    <a type="button" href="{{ url('admin/cupones/' . $entity->id . '/edit') }}" class="btn btn-info">
                                        <i class="fas fa-search"></i>
                                    </a>
                                    <a type="button" data-method="POST" onclick="return confirm('¿Seguro quieres borrar?. Esta acción no se puede deshacer')" href="{{ route('cupones.deleted', $entity->id) }}" class="btn btn-danger">
                                        <i class="fas fa-trash"></i>
                                    </a>
                                    <a type="button" data-method="GET" onclick="return confirm('¿Quieres marcar el cupon como canjeado?.')" href="{{ route('cupones.redeem', $entity->id) }}" class="btn btn-warning">
                                        <i class="fas fa-star"></i>
                                    </a>
                                    <a type="button" data-method="GET" onclick="return confirm('¿Quieres marcar el cupon como canjeado?.')" href="{{ route('cupones.redeemUser', [$entity->id, $entity->usuario_app_id]) }}" class="btn btn-danger">
                                        <i class="fas fa-star"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            
        </div>
    </div>
@stop

@section('js')
    <script> 
        $(function () {
            $("#cupones").DataTable({
                paging: true,
                language: {
                    url: "{{ asset('/vendor/datatables/locale/es-ar.json') }}",
                }
            });
        });
    </script>
@stop