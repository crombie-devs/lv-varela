@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="card-body">
    <div class="form-group">
        <div class="row">
            <div class="col-3">
                <div class="form-group">
                    {!! Form::label('canjeado', 'Canjeado') !!}<br>
                    {!! Form::checkbox('canjeado', null, (isset($entity) && $entity->canjeado == 1) ? 1 : 0, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('beneficio_id', 'Selecciona Beneficio') !!}<br>
        {!! Form::select('beneficio_id', $beneficios, null, ['class' => 'form-control', 'required' => true]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('usuario_app_id', 'Selecciona Usuario App') !!}<br>
        {!! Form::select('usuario_app_id', $usuarios, null, ['class' => 'form-control', 'required' => true]) !!}
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-6">
                {!! Form::label('fecha_canje', 'Fecha de Canje:') !!}
                {!! Form::hidden('fecha_canje', null) !!}
                <div class="input-group date" id="fecha_can" data-target-input="nearest">
                    <input type="text" name="fecha_can" class="form-control datetimepicker-input" data-target="#fecha_can" value="{{ (isset($entity->fecha_canje)) ? $entity->fecha_canje : '' }}">
                    <div class="input-group-append" data-target="#fecha_can" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card-footer">
    <button type="submit" class="btn btn-info pull-right">Guardar</button>
</div>

@section('js')
    <script> 
        $(function () {
            $.fn.bootstrapSwitch.defaults.onText = 'Si';
            $.fn.bootstrapSwitch.defaults.offText = 'No';
            $.fn.bootstrapSwitch.defaults.labelWidth = 0;
            $.fn.bootstrapSwitch.defaults.onColor = 'success';
            $.fn.bootstrapSwitch.defaults.offColor = 'danger';
            $.fn.bootstrapSwitch.defaults.inverse = true;

            $("[name='canjeado']").bootstrapSwitch();
            $('#fecha_can').datetimepicker({
                locale: moment.locale('es'),
                format: 'LL',
            });
            $('#fecha_can').on("change.datetimepicker", function (e) {
                let fecha_can = moment(e.date).format('YYYY-MM-DD'); 
                $('input[name="fecha_canje"]').val(fecha_can);
            });
        });
    </script>
@stop