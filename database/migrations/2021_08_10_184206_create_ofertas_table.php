<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofertas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('descripcion');
            $table->string('descripcion_corta')->nullable();
            $table->string('ganancia');
            $table->integer('categoria_id');
            $table->string('imagen_cuadro_url')->nullable();
            $table->string('imagen_cabecera_url')->nullable();
            $table->string('imagen_cuadro_src')->nullable();
            $table->string('imagen_cabecera_src')->nullable();
            $table->boolean('activa');
            $table->boolean('destacada');
            $table->date('fecha_desde');
            $table->date('fecha_hasta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ofertas');
    }
}
