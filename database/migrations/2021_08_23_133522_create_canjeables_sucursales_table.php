<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCanjeablesSucursalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('canjeables_sucursales', function (Blueprint $table) {
            $table->primary(['canjeable_id', 'sucursal_id']);
            $table->unsignedInteger('canjeable_id');
            $table->unsignedInteger('sucursal_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('canjeables_sucursales');
    }
}
