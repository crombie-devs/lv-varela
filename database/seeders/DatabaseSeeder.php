<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);

        \DB::table('usuarios_mobile')->insert([
            'id' => 'KzI8McL29LaRhrb6JP7GBmApmzB3',
            'email' => 'almadacristiand@gmail.com',
            'nombre' => '(usuario de prueba) Cristian',
            'apellido' => 'Almada',
            'fecha_nacimiento' => '1998-04-01',
            'barrio' => 'La Sirena',
            'rol' => 'ROLE_APP',
        ]);

        \DB::table('usuarios_mobile')->insert([
            'id' => 'oY4c3SJOyVacPQAP87a2QLrpUDi1',
            'email' => 'lazo@sismogames.com',
            'nombre' => '(usuario de prueba) Andrés',
            'apellido' => 'Rossi',
            'fecha_nacimiento' => '1979-09-29',
            'barrio' => 'Altamira',
            'rol' => 'ROLE_COMERCIO',
        ]);

        \DB::table('categorias')->insert([
            'nombre' => '(prueba) Todos',
            'descripcion' => 'Todos los productos',
        ]);

        \DB::table('categorias')->insert([
            'nombre' => '(prueba) Alimentos',
            'descripcion' => 'Alimentos varios',
        ]);

        \DB::table('categorias')->insert([
            'nombre' => '(prueba) Limpieza',
            'descripcion' => 'Artículos de limpieza',
        ]);

        \DB::table('comercios')->insert([
            'nombre' => '(prueba) Centro de Prueba Sismo',
            'descripcion' => 'Centro de prueba Sismo Games',
            'telefono' => '01130618062',
            'email' => 'prueba@sismogames.com',
            'web' => '-',
        ]);

        \DB::table('comercios')->insert([
            'nombre' => '(prueba) Café Martinez',
            'descripcion' => 'Café Martinez',
            'telefono' => '011065480',
            'email' => 'cafemartinez@hotmail.com',
            'web' => '-',
        ]);

        \DB::table('sucursales')->insert([
            'nombre' => '(prueba) Municipalidad de Florencio Varela',
            'direccion' => 'Municipalidad de Florencio Varela, Acceso a la Municipalidad, Florencio Varela, Partido de Florencio Varela, Buenos Aires, B1888, Argentina',
            'telefono' => '541152634600',
            'lat' => '-34.80431325',
            'long' => '-58.28053573447421',
            'comercio_id' => '1',
        ]);

        \DB::table('sucursales')->insert([
            'nombre' => '(prueba) Centro de Validación Varela + Cerca',
            'direccion' => 'Avenida General Fernández de la Cruz 2145, C1437GZA CABA, Argentina',
            'telefono' => '541152634600',
            'lat' => '-34.80118153047916',
            'long' => '-58.27410221099854',
            'comercio_id' => '1',
        ]);

        \DB::table('ofertas')->insert([
            'activa' => '1',
            'destacada' => '1',
            'nombre' => '(prueba) Galletitas Dulces DIA Frambuesa 140 Gr',
            'descripcion' => 'Llevate 2 paquetes al precio de uno',
            'descripcion_corta' => 'en Galletitas',
            'ganancia' => '2x1',
            'categoria_id' => '1',
            'fecha_desde' => '2021-07-10',
            'fecha_hasta' => '2022-08-01',
        ]);

        \DB::table('ofertas_sucursales')->insert([
            'oferta_id' => '1',
            'sucursal_id' => '1',
        ]);

        \DB::table('canjeables')->insert([
            'nombre' => '(prueba) Mochila Escolar Eco Cuero',
        ]);

        \DB::table('canjeables')->insert([
            'nombre' => '(prueba) Termo Stanley',
        ]);

        \DB::table('beneficios')->insert([
            'activa' => '1',
            'destacada' => '1',
            'nombre' => '(prueba) Cambio de aceite gratis',
            'descripcion' => 'Realizando dos services en Todo Motos recibis un cambio de aceite sin costo',
            'descripcion_corta' => 'Services en Todo Motos',
            'ganancia' => 'GRATIS',
            'alcance' => 'en cambio de aceite',
            'canjeable_id' => '1',
            'halcoins' => '10',
            'fecha_desde' => '2021-07-01',
            'fecha_hasta' => '2022-09-01',
        ]);

        \DB::table('dias')->insert([
            'id' => 1,
            'nombre' => 'lunes',
        ]);

        \DB::table('dias')->insert([
            'id' => 2,
            'nombre' => 'martes',
        ]);

        \DB::table('dias')->insert([
            'id' => 3,
            'nombre' => 'miércoles',
        ]);

        \DB::table('dias')->insert([
            'id' => 4,
            'nombre' => 'jueves',
        ]);

        \DB::table('dias')->insert([
            'id' => 5,
            'nombre' => 'viernes',
        ]);

        \DB::table('dias')->insert([
            'id' => 6,
            'nombre' => 'sabado',
        ]);

        \DB::table('dias')->insert([
            'id' => 7,
            'nombre' => 'domingo',
        ]);

        \DB::table('ofertas_dias')->insert([
            'oferta_id' => '1',
            'dia_id' => '1',
        ]);

        \DB::table('ofertas_dias')->insert([
            'oferta_id' => '1',
            'dia_id' => '3',
        ]);

        \DB::table('canjeables_sucursales')->insert([
            'canjeable_id' => '1',
            'sucursal_id' => '1',
        ]);

        \DB::table('canjeables_sucursales')->insert([
            'canjeable_id' => '1',
            'sucursal_id' => '2',
        ]);

        \DB::table('canjeables_sucursales')->insert([
            'canjeable_id' => '2',
            'sucursal_id' => '1',
        ]);
    }
}
