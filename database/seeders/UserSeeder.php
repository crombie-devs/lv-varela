<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'App Varela',
            'email' => 'app@varela.com',
            'password' => bcrypt('varelaMasCerca21'),
            'rol' => 'ROLE_APP'
        ])->assignRole('ROLE_ADMIN');
        
        User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('Secret1234'),
            'rol' => 'ROLE_ADMIN'
        ])->assignRole('ROLE_ADMIN');

        User::create([
            'name' => 'Comercio Varela',
            'email' => 'comercio@varela.com',
            'password' => bcrypt('varelaMasCerca21'),
            'rol' => 'ROLE_COMERCIO',
            'comercio_id' => '1'
        ])->assignRole('ROLE_COMERCIO');

        // User::create([
        //     'name' => '(usuario de prueba) Max Shtefec',
        //     'email' => 'max.shtefec@gmail.com',
        //     'password' => bcrypt('Passw0rd'),
        //     'rol' => 'ROLE_ADMIN'
        // ])->assignRole('ROLE_ADMIN');

        User::create([
            'name' => '(usuario de prueba) Eira Martinez',
            'email' => 'eira.martinez@4rsoluciones.com',
            'password' => bcrypt('Passw0rd'),
            'rol' => 'ROLE_COMERCIO',
            'comercio_id' => '1'
        ])->assignRole('ROLE_COMERCIO');
    }
}
