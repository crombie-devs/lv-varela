<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::create(['name' => 'ROLE_ADMIN']);
        $role_comercio = Role::create(['name' => 'ROLE_COMERCIO']);

        Permission::create(['name' => 'backend.index'])->assignRole($role_admin);
        Permission::create(['name' => 'mobile.index'])->assignRole($role_admin);
        Permission::create(['name' => 'comercios.index'])->assignRole($role_admin);
        Permission::create(['name' => 'sucursales.index'])->syncRoles([$role_admin, $role_comercio]);
        Permission::create(['name' => 'sucursales.create'])->assignRole($role_admin);
        Permission::create(['name' => 'categorias.index'])->assignRole($role_admin);
        Permission::create(['name' => 'ofertas.index'])->syncRoles([$role_admin, $role_comercio]);
        Permission::create(['name' => 'ofertas.create'])->assignRole($role_admin);
        Permission::create(['name' => 'canjeables.index'])->assignRole($role_admin);
        Permission::create(['name' => 'beneficios.index'])->syncRoles([$role_admin, $role_comercio]);
        Permission::create(['name' => 'beneficios.create'])->assignRole($role_admin);
        Permission::create(['name' => 'banners.index'])->assignRole($role_admin);
        Permission::create(['name' => 'notificaciones.index'])->assignRole($role_admin);
        Permission::create(['name' => 'contactos.index'])->assignRole($role_admin);
        Permission::create(['name' => 'beneficios.canjeables'])->assignRole($role_comercio);
    }
}
