# Descripción Corta:
Es un proyecto de Crombie que fue copiado de otro proyecto denominado Lazo. Pretende proporcionar de funcionalidades similares.

# Requisitos:
* `Mysql`.
* `PHP v7.1`.
* `Laravel v8`.

# Prueba a Levantar las Migraciones:
* Antes que nada no te olvides de: `composer install`
* Crea la base de datos con el mismo nombre que figura en el .env y dale al comando, `php artisan migrate`

# Datos de Configuración / Environment:
* Se encuentra suelto: `.env`

* `DB_DATABASE=lv_varela`
* `DB_USERNAME=root`
* `DB_PASSWORD=''`
* `DB_HOST=localhost`
* `DB_PORT=3306`

# Datos de Acceso:
* **Usuario Administrador**
* **URL de Acceso:** [http://localhost:8000](http://localhost:8000).
* **Usuario:** admin@gmail.com.
* **Contraseña:** Secret1234.
* **O crea un usuario nuevo.**